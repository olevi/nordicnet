Short description:

  Tool to install FDS/ESP using simplified install profile format (with greater flexibility) and to apply customizations to
an FDS/ESP installation. (ESPDeploy now inlcudes new version of mexec (and task for installing))

See the documentation in the doc dir for more information

Customizations include: 
  apply overlay tarball/zip
  apply hotfix
  regex patch file
  xml patch file
  'unix patch' file
  create automaton
  install and configure mexec

Mexec:
  - mexec is a very flexible method of executing commands on esp hosts (allowing targting command by services running on host)
    - also allows copying files to hosts, and many more things
    - (execution of commands can be performed asynchronously (makes those cp -r $FASTSEARCH/data /backup/path commands nice and fast))
    - (this is a very advanced fds_ssh program that is capable of handling a lot of extra goodies)
  - mexec has been reimplemented in python and now is much faster and usable
  - the <Mexec> task in espdeploy will install and configure mexec for a cluster
  - running mexec -h (after installation) will show help on all that mexec is
  - see http://enterprise.cvs.fast.no/cgi-bin/cgi/viewcvs.cgi/customers/scopus.new/deploy/configs/deploy-mexec.xml
    - this is an example deployment configuration to deploy mexec


Examples:
  - http://enterprise.cvs.fast.no/cgi-bin/cgi/viewcvs.cgi/customers/scopus.new/deploy/
    - this is the deployment configurations and install profiles for scopus
    - these config files define 3 different types of installations
    - <include> tags are used to do common things for all installations
    - this is installing instream 4 (minor changes would be needed to install esp 5)

Created by : Tim Smith
$Author: tsmith $
$Date: 2006/12/08 20:12:14 $
$Revision: 1.3 $
$Source: /cvs/scs/esp5tools/operation/espdeploy/README.txt,v $

Copyright (C) 2000 - 2006
Fast Search & Transfer ASA

