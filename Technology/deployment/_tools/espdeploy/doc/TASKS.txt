
Table of Contents:
  1 Task Definition
    1.1 Available Tasks


1 Task Definition

  ESP Deploy uses Tasks to perform configuration/installation of
  custom code over top of an ESP/FDS deployment.  Each task will
  result in remote commands being executed and/or files being copied
  to the remote hosts.

1.1 Available Tasks

  The following tasks are available:
   - License        - configure fastsearch.lic
   - Copy           - copy files from deploy host to remote hosts
   - Overlay        - apply a zip/tar/jar overlay
   - Hotfix         - apply hotfixes
   - Chmod          - change permissions for a file/files
   - Execute        - remotly execute arbitrary commands
   - Makeaut        - create automatons
   - RegexPatch     - apply regexes to a file
   - FilePatch      - run "patch" against a file
   - XmlPatch       - patch xml config files
   - TopFdispatch   - set up custom engines files for topfdispatch
   - TopCDProxy     - setup a Top level CD Proxy
   - PipelineConfig - patch the PipelineConfig
   - Install        - install ESP/FDS - this facilitates running the ESP
		      installation in parallel via threads, as well as to
		      target just a few hosts for installation (using
		      --hosts command line option)
   - Mexec          - setup mexec, a tool for executing commands across
		      machines in a cluster
   - DistLink	    - configure DistLink (a custom tool for scopus)
   - Fidelity	    - conifigure Fidelity (a custom tool for elsevier)
   

  Documentation is available for each task in task/<task>.txt
  Proper task xml format for use in deployment config and examples are
  provided here
