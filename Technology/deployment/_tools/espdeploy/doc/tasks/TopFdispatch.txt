* TopFdispatch Task (<TopFdispatch>):
  This task will allow defining your own topfdispatcher (using a custom grouping of engines/datasets) instead of relying on searchctrl to manage things
   The resulting engines files can then be user modified to add/remove specific engines (restart of topfdispatch is all thats needed to take effect)
   This task should be accompanied with a NodeConf.xml xml patch to add in the appropriate services
   This is only known to work on esp 4.x currently

  Attributes:
    @cluster             -- the name of the search cluster to use (default: webcluster)
    @baseport            -- the baseport for the topfdispatcher (default: 15150)
    @maxoffset           -- the maximum offset that result set can be paged into (default: 4000)	
    @replace-searchctrl  -- if true, will replace out searchctrl as well (move it to a higher port) (default: true)
    @searchctrl-baseport -- the base port to give searchctrl (this really needs to run (even though it won't do anything)) (default: 28150)
    @partition-id        -- the partition id (default: 0)     
   
  Sub-Elements:
    <dataset>  -- define a dataset to add to the engines configuration (all search engines in the system will be added to each dataset)
      @refcost -- the refcost of this dataset (setting this to 0 will mean this dataset will only be searched if using the "dataset" query parameter) (default: 0)
      @engine-refcost -- this is the default refcost for engines in this dataset (default: 1)
       
      <row>  -- this will allow defining new refcosts for engines on a row by row basis (the first <row> element is row 1, the second is row 2, and so on)
        @refcost  -- override engine-refcost for engines in this search row with this value

    <dataset-foreach-row> -- this will define a dataset for each row in the system (if there are 3 rows, 3 unique datasets will be created by this element)
                             for each dataset, one row will be "selected"
      @refcost                  -- the refcost for this dataset
      @selected-row-refcost     -- this is the refcost for engines in the "selected" row
      @non-selected-row-refcost -- this is the refcost for engines in the "non-selected" row            

  Example:
    <!-- setup top fdispatch if this is a multi-node install -->
    <TopFdispatch group="top-fdispatch" cluster="webcluster" baseport="15150" maxoffset="5000" desc="Setup Engines For Top Dispatchers">
      <!-- default dataset, prefer all rows equally (in theory) -->
      <dataset refcost="1" engine-refcost="1"/>

      <!-- prefer row 1, failover to row 2, only failover to row 0 if needed -->
      <dataset refcost="0" engine-refcost="0">
        <row refcost="100"/> <!-- only fail over to row 0 if really needed -->
        <row refcost="1"/>   <!-- favor row 1 -->
        <row refcost="10"/>  <!-- failover to row 2 if row 1 is having trouble -->
      </dataset>

      <!-- prefer row 2, failover to row 1, only failover to row 0 if needed -->
      <dataset refcost="0" engine-refcost="0">
        <row refcost="100"/> <!-- only fail over to row 0 if really needed -->
        <row refcost="10"/>  <!-- failover to row 2 if row 1 is having trouble -->
        <row refcost="1"/>   <!-- favor row 1 -->
      </dataset>

      <!-- create dataset for each row (no failover to opposite rows) -->
      <dataset-foreach-row refcost="0" selected-row-refcost="1" non-selected-row-refcost="0"/>

      <!-- create dataset for each row, favoring 1 row for each dataset, allowing failover to opposite rows -->
      <dataset-foreach-row refcost="0" selected-row-refcost="1" non-selected-row-refcost="10"/>
    </TopFdispatch>
