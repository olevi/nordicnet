* SetProperty Task (<SetProperty>):
  This task will set the value of a property in a Java property style 
  configuration file.  
  If the named property doesn't exist, a new property will be appended to the 
  file and the value assigned to it.
  
  Any environment variables in the 'value' and 'comment' attributes will be 
  expanded in the task.

  Required Attributes:
    @prop-file    - File containing the property you wish to set. If this path is
              relative, it is relative to $ESP_INSTALL_DIR.
    @property   - Name of property you wish to set. 
    @value     - Value of property you wish to set.
   
  Optional Attributes:
      @separator  - Separator character(s) between the property name and its value.
                 If not set, default is '='.
    @comment   - Insert commented text previous to the line containing the 
              property you are setting (uses '#' to denote a comment).
              
  Example:
    <SetProperty group="all" 
             prop-file="j2ee/server/vespa/deploy/bizmanweb.war/bizmanweb.properties" 
             property="search.price.field"
             value="currentprice"
             comment="changed property to currentprice"
             desc="Change property value to current price."/>

  Use Case:
   ImPulse's bizman properties uses field name 'currprice' to populate its
   search.price.field.  Need to change value to 'currentprice'.