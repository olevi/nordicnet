* Mexec Task (<Mexec>) [ unix only ]:
  This task will install mexec on the system and configure a special bashrc file containing desired exports and aliases that make life livable.
  Mexec is a script that allows issuing commands/copying files to specific types of nodes (or the entire cluster)

  Optional Attributes:
    @prefix  -- this is a short (2-3 char recommended) representation of the name of the cluster (default: esp)
                (mexec command will be symlinked in the following way  <prefix>_[<group>]_<command>)
    @script-file -- this is the location of the mexec.py script on the deploy machine (default: $ESP_DEPLOY_DIR/scripts/mexec.py)
    @install-path -- this is the directory to install mexec config files, script, and symlinks to (default: $ESP_INSTALL_DIR)
    @update-bashrc -- if true, $HOME/.bashrc will be updated to source the custom bashrc file this task creates (default: false)

  Sub-Elements:
    <ps1>  -- allows configuring the system prompt
      @color  -- the color for the command prompt (ex: green, yellow, magenta, blue, red, brightgreen, etc)
      @header -- this is what will be displayed in the terminal's title bar
      @host-ext -- this extension will be appended to the host's name on the command prompt

    <export>  -- define a bash export
      @name  -- the name of the environment variable
      @value -- the value to give the environment variable

    <alias> -- define an alias
      @name  -- the name of the alias
      @value -- what the alias will result in calling

    <group> -- define a group mapping to a short prefix for command execution (see mexec -h)
      @name   -- this is the name of the espdeploy group name
      @prefix -- this is the prefix mexec will use for hosts in this group
    
    <startorder> -- define the start order when running <prefix>_start
      <prefix>     -- content for this sub element is the prefix for a
		      group of nodes. nodes will be started in the
		      order these are specified

  Example:
    <Mexec group="all" prefix="$MEXEC_CLUSTER" script-file="$ESP_DEPLOY_DIR/scripts/mexec.py" install-path="/home/search" update-bashrc="true">
      <!-- define what our command line prompt will be like -->
      <ps1 color="$COLOR" header="$SCOPUS_RELEASE $ENVIRONMENT $CLUSTER_TYPE" host-ext=".bos3"/>

      <!-- define exports needed by fds -->   
      <export name="FASTSEARCH"      value="$ESP_INSTALL_DIR"/>
      <export name="PATH"            value="\$PATH:\$FASTSEARCH/bin"/>
      <export name="LIBPATH"         value="\$PATH:\$FASTSEARCH/lib"/>
      <export name="SHLIB_PATH"      value="\$PATH:\$FASTSEARCH/lib"/>
      <export name="LD_LIBRARY_PATH" value="\$LD_LIBRARY_PATH:\$PATH:\$FASTSEARCH/lib"/>
      <export name="OMNIORB_CONFIG"  value="\$FASTSEARCH/etc/omniorb.cfg"/>
      <export name="LM_LICENSE_FILE" value="\$FASTSEARCH/etc/fastsearch.lic"/>
      <export name="RSYNC_RSH"       value="ssh -o cipher=blowfish"/>
      <export name="TMPDIR"          value="/usr/fast/tmp"/>

      <!-- define bash aliases -->
      <alias name="ls"     value="ls --color"/>
      <alias name="ll"     value="ls --color -l"/>
      <alias name="python" value="cobra"/>
      <alias name="ds"     value="cd \$FASTSEARCH"/>
      <alias name="nano"   value="nano -w"/>
      <alias name="grep"   value="grep --color=auto"/>

      <!-- define our host groupings -->
      <group name="search-engine-index"    prefix="in"/>
      <group name="search-engine-search"   prefix="sr"/>
      <group name="configuration-service"  prefix="as"/>
      <group name="document-processor"     prefix="pr"/>
      <group name="query-result-processor" prefix="ds"/>
      <group name="status-service"         prefix="st"/>
      <group name="distlink-master"        prefix="dl"/>

      <!-- define the start order for nodes -->
      <startorder>
        <prefix>as</prefix>
	<prefix>st</prefix>
	<prefix>pr</prefix>
	<prefix>sr</prefix>
	<prefix>ds</prefix>
      </startorder>

    </Mexec>
