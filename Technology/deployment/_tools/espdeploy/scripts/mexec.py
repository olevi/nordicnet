#!/usr/bin/env python
import re, os, sys, string, getopt, shutil
import threading
KeyInt = KeyboardInterrupt #Short alias for this exception
OptErr = getopt.GetoptError


#Load constants (via environment variables
ETC      = os.environ.get("CLUSTER_ETC", ".")
COLS     = os.environ.get("CLUSTER_COLS", "0").split()
ROWS     = os.environ.get("CLUSTER_ROWS", "0").split()
I_ROWS   = os.environ.get("INDEX_ROWS", os.environ.get("CLUSTER_ROWS", "0")).split()
COLOR    = int(os.environ.get("CLUSTER_COLOR", "32"))
USECOLOR = sys.platform != "win32" and os.isatty(sys.stdout.fileno())

HOST_ACTIONS    = [ "ping", "test", "exec", "scp", "hostlist", "hostkeys" ]
CLUSTER_ACTIONS = [ "start", "rtsadmin", "dump", "engines" ] 

#COLORS
RED     = 31
GREEN   = 32
YELLOW  = 33
BLUE    = 34
MAGENTA = 35
CYAN    = 36
WHITE   = 37

#Commands
PING_COMMAND = 'ping -w 1 -c 1 %s | grep "icmp_seq" | awk \'{ print $8 }\' | awk -F= \'{print $2}\''

#DEFAULTS
DEFAULT_BINPATH = os.path.join( os.environ.get("HOME", "."), "bin" )

class CommandThread(threading.Thread):
  def __init__(self, host, command):
     self.host = host
     self.command = command
     threading.Thread.__init__(self)
  def run( self ):
     host_execute(self.host, self.command)
  

def usage(binary, action, error = None):
  if action in (None, "links"):
    print "Usage: %s [options]" % binary
    print ""
    print "Options:"
    print "  -h, --help            Show this information"
    print "  -l, --create-links    Create symlinks for mexec"
    print "  -b, --bin-path=<PATH> Binary path where mexec is installed (DEFAULT: %s)" % DEFAULT_BINPATH
    print "  -e, --etc-path=<PATH> Config path for host-group lists (Overrides $CLUSTER_ETC)"
    print "  -r, --role=<ROLE>     Server Role (Required)"
    print "  -c, --action=<ACTION> Action to perform (One of: exec, scp, hostlist, ping, start, ...)"
    print ""
    print "NOTE:"
    print "  If using --action (or -c) command line switch, do a `mexec --action ACTION -h` for additional action"
    print " specific command line options."
    print ""
  elif action in HOST_ACTIONS:
    extra = ""
    if action == "scp":    extra = "[[user@]host1:]file1 [...] [[user@]host2:]file2"
    elif action == "exec": extra = "\"<command>\""
    print "Usage: %s [options] %s" % (binary, extra)
    print ""
    print "Options:"
    print " -h   Show this information"
    print " -q   Turn down that racket you kids"
    print " -Q   Complete silence is golden"
    if action not in ("hostlist", "ping"):
      print " -s   Don't use ssh key"
    if action in ("exec", "scp"):
      print " -t   Allocate remote pseudo terminal (enables stdin for remote commands)"
      print " -a   Run commands asynchronously (will wait till all commands return)"
      print ""
      print "Command Substitution:"
      print "  All occurrances of %HOST% in the command/filepath will be replaced"
      print " with the host currently being worked on"
    
    print ""

    if action == "scp":
      print "Examples:"
      print " # Copy filename to all hosts"
      print "   %s filename %%HOST%%:./" % binary
      print " # Copy filename from each host to filename_<source_hostname>"
      print "   %s %%HOST%%:filename filename_%%HOST%% " % binary
      print ""
    elif action == "exec":
      print "Examples:"
      print " # Edit NodeConf on each host (Note the -t flag)"
      print "   %s -t nano /usr/fast/datasearch/etc/NodeConf.xml" % binary
      print " # Stop Datasearch"
      print "   %s -a nctrl stop" % binary
      print " # View disk utilization across all machines for partition"
      print "   %s \"df -h | grep /usr/vmware\"" % binary
      print ""
    
  elif action == "start":
    print "Usage: %s [options]" % binary
    print ""
    print "Action: Start Datasearch Across Cluster"
    print ""
  elif action == "engines":
    print "Usage: %s [options] [column [column ...]]" % binary
    print ""
    print "Action: View Search Engine Status"
    print ""
  elif action == "rtsadmin":
    print "Usage: %s [-h|--help] <rts-command> [rts-options]" % binary
    print ""
    print "Action: Run rtsadmin command against all indexers"
    print "        Run rtsadmin -h for available commands and options"
    print ""
  elif action == "dump":
    print "Usage: %s [-h|--help] <sum-field> [<sum-field> ...]" % binary
    print ""
    print "Action: Dump document summaries to docsum-dump.dsum in current directory"
    print "        Output file will be 1 line per doc, semi-colon separated"
    print ""
    print "NOTE: Requires pydataset is installed"
    print ""
    print "Example: %s bsumeid bsumnumcitedby" % binary
    print ""

  print "Generic MEXEC Information:"
  print "  Mexec is a tool for remotely executing commands across a datasearch cluster"
  print "  Multiple symlinks are created to allow running mexec in different modes"
  print "  To use mexec without symlinks, use the --role and --action command line options"
  print ""
  print "  There are 2 forms of commands that mexec can perform:"
  print "    Cluster Level Actions: Run a specialized global action"
  print "                           Take the form CLUSTERID_COMMAND [arguments]"
  print "    Host-Group Actions:    Run an action across all hosts in host group"
  print "                           Take the form CLUSTERID_[HOST-GROUP-PREFIX_]_COMMAND [arguments]"
  print ""
  print "  Cluster Level Actions:"
  print "    start    -- Start datasearch for cluster"
  print "    dump     -- Dump and merge document summaries"
  print "    rtsadmin -- Run rtsadmin command across all indexers"
  print ""
  print "  Host-Group Actions:"
  print "    exec     -- Remotely execute a command across all hosts in group"
  print "    scp      -- Copy files to/from hosts in group"
  print "    test     -- Test that remote commands can be executed"
  print "    ping     -- Ping all hosts in group"
  print "    hostlist -- Print out all hosts in group"
  print "    hostkeys -- Print out hosts keys for all hosts in group"
  print ""
  print "  Environment Variables Used:"
  print "    CLUSTER_ETC   -- the location of host group lists"
  print "    CLUSTER_COLOR -- the linux console color to use for pretty output"
  print "    CLUSTER_COLS  -- a space sep list of columns in cluster (0 indexed)"
  print "    CLUSTER_ROWS  -- a space sep list of rows in cluster (0 indexed)"
  print "    INDEX_ROWS    -- a space sep list of indexer rows in cluster (0 indexed)"
  print ""

  if error != None:
    print "ERROR:", str(error)
    print ""
  
  raise SystemExit(1)

def main():
  binary  = os.path.basename(sys.argv[0]).split(".")[0]
  script  = binary.split("_")
  config  = "_".join(script[:-1] + [ "cluster.txt"] )
  cluster = script[0]
  action  = script[-1]
  argv    = sys.argv[1:]
  binpath = DEFAULT_BINPATH
  etcpath = ETC
  options = { }

  if binary == "mexec": action, cluster, config = None, None, None

  try:              opts, args = getopt.getopt(sys.argv[1:], "hb:e:lc:r:atnqQs", ["help", "bin-path=", "etc-path=", "create-links", "action=", "role="])
  except OptErr, e: usage(binary, None, "Command Line Error: %s" % e)
      
  for o, a in opts:
    if o in ("-h", "--help"):           usage(binary, action)
    elif o in ("-b", "--bin-path"):     binpath = a
    elif o in ("-e", "--etc-path"):     etcpath = a
    elif o in ("-l", "--create-links"): action = "links"
    elif o in ("-c", "--action"):       action = a
    elif o in ("-r", "--role"):         cluster, config = a.split("_")[0], "%s_cluster.txt" % a
    elif o == "-a": options['async']   = True
    elif o == "-t": options['useterm'] = True
    elif o == "-n": options['noinput'] = True
    elif o == "-s": options['nokey']   = True
    elif o == "-q": options['quiet']   = 1
    elif o == "-Q": options['quiet']   = 2

  #Do Some Validation
  if action in ("scp", ) and len(args) < 2: usage(binary, action)
  if action in ("rtsadmin", "dump") and not len(args): usage(binary, action)
  if action in ("dump", "engines", "start") and cluster is None: usage(binary, action, "Must use --role command line option")

  #Read hostlist (if needed)
  if action in HOST_ACTIONS:
    if config is None: usage(binary, action, "Must use --role command line option")
    hosts = get_hostlist(config)      
    if action != "hostkeys" and options.get("quiet") != 2:
      output( ("*** Using Machine List: %s" % config, WHITE, True), None)

  if action   == "links":    build_links(binary, etcpath, binpath)
  elif action == "start":    start_fds(options, cluster)
  elif action == "rtsadmin": rtsadmin(cluster, args)
  elif action == "dump":     dump_docsums(options, cluster, args)
  elif action == "engines":  show_engines(options, cluster, args)      
  elif action == "scp":      run_scp(options,  hosts, args)
  elif action == "ping":     run_ping(hosts)
  elif action == "test":     run_test(options, hosts)
  elif action == "exec":     run_exec(options, hosts, args)
  elif action == "hostlist": show_hostlist(hosts)
  elif action == "hostkeys": show_hostkeys(options, hosts)
  else:                      usage(binary, action)
  output("\n")

#MEXEC DIRECT -- Create symlinks used by mexec
def build_links(mexec, etcpath, binpath):
  output( ("*** Building mexec Symlinks ***", WHITE, True), None)     
  binary = os.path.join(binpath, mexec)
  #Create paths needed
  for path in [ binpath, os.path.join(binpath, "_linked_") ]:
    try:    os.makedirs(path)
    except: pass

  #Remove current links
  for link in map(lambda x: os.path.join(path, x), os.listdir(path)):
    if os.path.islink(link):
      os.unlink(link)

  #Link host-group actions
  prefixes = [ x[:-12] for x in os.listdir( etcpath ) if x.endswith("_cluster.txt") ]
  for prefix in prefixes:
    for action in HOST_ACTIONS:
      os.symlink(binary, os.path.join(binpath, "_linked_", "%s_%s" % (prefix, action)))

  #Link global actions
  for prefix in filter(lambda x: x.find("_") == -1, prefixes):
    for action in CLUSTER_ACTIONS:
      os.symlink(binary, os.path.join(binpath, "_linked_", "%s_%s" % (prefix, action)))


######################################
####### CLUSTER LEVEL ACTIONS ########
######################################

def start_fds(options, cluster):
  options['async'] = True
  started = { }
  for entry in get_startorder( cluster ):
    hosts = get_hostlist("%s_%s_cluster.txt" % (cluster, entry))
    hosts = filter(lambda x: not started.has_key(x), hosts) #filter out hosts that have already been started
    if len(hosts):
      output( ("*** Starting %s nodes ***" % entry, WHITE, True), None )
      run_exec(options, hosts, ["nctrl start"])
      for host in hosts: started[host] = 1

  #start up any hosts that were missed
  hosts = get_hostlist("%s_cluster.txt" % cluster)
  hosts = filter(lambda x: not started.has_key(x), hosts) #filter out hosts that have already been started
  if len(hosts):
    output( ("*** Starting any missed nodes nodes ***", WHITE, True), None )
    run_exec(options, hosts, ["nctrl start"])

def rtsadmin(cluster, argv):
  if os.path.isfile( os.path.join(os.environ.get("FASTSEARCH", "."), "bin", "indexeradmin") ):
    os.system("indexeradmin -a %s" % " ".join(argv))
  else:
    host = get_hostlist("%s_as_cluster.txt" % cluster)[0]
    output(("*** Running ", WHITE, True), ("rtsadmin ", CYAN), (host, COLOR),
           " 16099 webcluster X X %s " % " ".join(argv), ("***", WHITE, True), None)
    for col in COLS:
      for row in I_ROWS:
        output( ("Column %02d, Row %02d: " % (int(col), int(row)), COLOR) )
        os.system("rtsadmin %s 16099 webcluster %s %s %s" % (host, col, row, " ".join(argv)))

def dump_docsums(options, cluster, argv):
  if len(ROWS) > 1: hostlist = "%s_sr_r0_cluster.txt" % cluster
  else:             hostlist = "%s_sr_cluster.txt" % cluster

  if os.system("which docsum-tool 1>/dev/null 2>&1"):
    output( ("*** docsum-tool not installed. Cannot dump document summareis ***", RED, True), None )
    return

  options['async'] = True
  hosts   = get_hostlist(hostlist)
  tempdir = "/usr/fast/.docsum-dump-temp"

  output( ("*** Creating Temporary Directory  ***", WHITE, True), None )
  run_exec(options, hosts, [ "rm -rf %s; mkdir -p %s" % (tempdir, tempdir) ]) 
  os.system("rm -rf %s; mkdir -p %s" % (tempdir, tempdir))
    
  output( ("*** Dumping Document Summaries    ***", WHITE, True), None )
  run_exec(options, hosts, [ "docsum-tool -z -o %s -b -m -u \";\" -F \"%s\"" % (tempdir, " ".join(argv)) ])

  output( ("*** Retreiving Document Summaries ***", WHITE, True), None )
  run_scp(options, hosts, [ "%%HOST%%:%s/merged_unified.txt %s/%%HOST%%_dump.dsum" % (tempdir, tempdir) ])

  output( ("*** Merging Document Summaries    ***", WHITE, True), None )
  os.system("cat %s/*.dsum > docsum-dump.dsum" % tempdir)

  output( ("*** Cleaning Up                   ***", WHITE, True), None )
  run_exec(options, hosts, [ "rm -rf %s" % tempdir ])
  os.system("rm -rf %s" % tempdir)
  output( ("*** Output File: docsum-dump.dsum ***", WHITE, True), None )
    
def show_engines(options, cluster, argv):
  columns = [ int(x) for x in argv if x.isdigit() ]
  if len(columns):
    output(("*** Fetching Engine Status For Columns %s ***" % ", ".join(map(str, columns)), WHITE, True), None, None)
  else:
    output(("*** Fetching Engine Status For All Search Nodes ***", WHITE, True), None, None)
  
  layout = { }
  re_colid = re.compile("columnrowid\s*=\s*\"(\d+)-(\d+)\"", re.M)
  for host in get_hostlist("%s_sr_cluster.txt" % cluster):
    command = "cat $FASTSEARCH/etc/searchrc-1.xml $FASTSEARCH/var/searchctrl/etc/enginesrc-15600"
    data = execute( get_ssh_command(options, host, command) )
    try:    col, row = re_colid.search(data).groups()
    except: col, row = -1, -1
    col, row = int(col), int(row)
      
    if not layout.has_key(col): layout[col] = { }
    layout[col][host] = re.findall("engine [^:]*:\d+ (\d+) (\d+) (\d+)", data)
    layout[col][host].sort()
      
  cols = layout.keys()
  cols.sort()
  for col in cols:
    if len(columns) and col not in columns: continue
    output(("*** Column %02d Engine Status ***" % col, WHITE, True), None)
    rows = layout[col].keys()
    rows.sort()

    output( ("%-15s" % "Index", WHITE, True) )
    for row in range(len(rows)): output( ("Row %d  " % row, WHITE, True) )
    output(None)

    for idx in range(len(layout[col][rows[0]])):
      pc, pr, pa = layout[col][rows[0]][idx]
      index = "index_%s_%s" % (pc, pr)
      output( ("%-15s" % index, CYAN) )

      for host in rows:
        if int(layout[col][host][idx][2]): val = "up"
        else:                              val = "down"
        output( ("%-7s" % val, COLOR) )
      output(None)
    output(None)

###################################
####### HOST GROUP ACTIONS ########
###################################

##THE ACTIONS THAT CAN BE PERFORMED
def show_hostlist(hostlist):
  output( ("\n".join(hostlist), COLOR), None )

def show_hostkeys(options, hostlist):
  for host in hostlist:
    ipaddress, key = None, None
    
    #Get ip address
    data = execute( get_ssh_command(options, host, "/sbin/ifconfig eth0") ).strip()
    loc = data.find("inet addr:")
    if loc > -1: ipaddress = data[loc + 10:data.find(" ", loc + 10)]

    #Get host's public rsa key
    data = execute( get_ssh_command(options, host, "cat /etc/ssh/ssh_host_rsa_key.pub") ).strip()
    for line in data.split("\n"):
      if line.startswith("ssh-rsa"): key = line

    #Display the keys to standard out
    if key == None:       continue
    if ipaddress != None: output(host, ",", ipaddress, " ", key, None)  
    output(host, " ", key, None)

def run_ping(hostlist):
  for host in hostlist:
    data = execute(PING_COMMAND % host).strip()
    output(("Ping ", WHITE, True), ("%-20s : " % shorthost(host), COLOR))
    if len(data): output(("OK", WHITE, True), (" %s" % data, WHITE), None)
    else:         output(("FAILED", YELLOW, True), None)

def run_test(options, hostlist):
  for host in hostlist:
    data = execute( get_ssh_command(options, host, "echo TEST") ).strip()
    output( ("Test ", WHITE, True), ("%-20s : " % shorthost(host), COLOR) )
    if data.find("TEST") > -1: output( ("OK",     WHITE,  True), None )
    else:                      output( ("FAILED", YELLOW, True), None )

def run_exec(options, hostlist, argv):
  command_list = [ ]
  for host in hostlist:
    command = get_ssh_command(options, host, prepare_args(host, argv) )
    title   = prepare_args(highlight(host, COLOR), argv)
    command_list.append( (host, title, command) )

  run_commands(options, command_list)

def win_host(host):
   return "\\\\" + host
   
#########################
#####UTILITY METHODS#####
#########################

def shorthost(host):
  return host.split(".")[0]

  
def ssh_escape(command):
  return '"%s"' % command.replace("\\", "\\\\").replace('"', '\\"')

#load the start order from etc file
def get_startorder(cluster):
  filepath = os.path.join(ETC, "%s_startorder.txt" % cluster)
  if os.access(filepath, os.R_OK):
    fp = file(filepath, "rb")
    order = filter(None, map(string.strip, fp))
    fp.close()
    return order
  else:
    output( ("*** Failed to load startorder from '%s_startorder.txt' (will use default) ***" % cluster, RED, True), None )
    return [ "as", "st", "pr", "sr", "ds" ]

#READ HOST LIST FROM etc FILE
def get_hostlist(filename):
  hosts = { }
  filepath = os.path.join(ETC, filename)
  output (("Reading hostlist from file: %s" % filepath,GREEN, True), None)
  if os.access(filepath, os.R_OK):
    fp = file(filepath, "r")
    for h in map(string.strip, fp):
      if h.startswith("#include"):
        hosts.update( dict( map(lambda x: (x, 1), get_hostlist(h.split()[1])) ) )
      elif len(h) and not h.startswith("#"):
        hosts[h] = 1      
  else:
    output( ("*** Failed to load host list file '%s' ***" % filename, RED, True), None )

  hostlist = hosts.keys()
  hostlist.sort()
  return hostlist

#Colorized printing support
def highlight(value, color, bold = False):
  if len(value) and USECOLOR:
    if bold: commands = [ str(color), "1" ]
    else:    commands = [ str(color) ]
    return "\033[%sm%s\033[0m" % (";".join(commands), value)
  else:
    return value

def output(*argv):
  for arg in argv:
    if type(arg) in (list, tuple):
      value, color, bold = "", WHITE, False
      if len(arg):     value = arg[0]
      if len(arg) > 1: color = arg[1]
      if len(arg) > 2: bold  = arg[2]
      sys.stdout.write( highlight(value, color, bold) )
    elif arg == None:
      sys.stdout.write("\n")
    else:
      sys.stdout.write( str(arg) )

  sys.stdout.flush()

  
def run_commands(options, command_list):
  if options.get("async"): exectitle = "*** Will Execute "
  else:                    exectitle = "*** Executing "
  
  for onhost, title, command in command_list:
    if not options.get("quiet"):
      output( (exectitle, WHITE, True), title, (" on %s ***" % onhost, WHITE, True), None)
    if not options.get("async"):
      if options.get("useterm"): os.system(command)
      else:                      host_execute(onhost, command)

  #Fork off and wait if asyncing
  if options.get("async"):
    if sys.platform.find("win") == -1:
    
      pids = [ ]
      for onhost, title, command in command_list:
        pid = os.fork()
        if pid == 0:
          host_execute(onhost, command)
          os._exit(0)
        else:
          pids.append( pid )
        
      for pid in pids:
        os.waitpid(pid, 0)

    else:
      for onhost, title, command in command_list:
        CommandThread(onhost, command).start()
      
        
#EXECUTION METHODS
if sys.platform.find("win") == -1:
  def run_scp(options, hostlist, argv):
    prog, command_list = 'scp ', [ ]
    for host in hostlist:
      command  = prog + '-o "StrictHostKeyChecking=no" ' + prepare_args(host, argv)
      title    = highlight(prog, CYAN) + prepare_args(highlight(host, COLOR), argv)
      command_list.append( ("localhost", title, command) )
    run_commands(options, command_list)

  def get_ssh_command(options, host, command):
    r_command = "ssh -A "
    if options.get("noinput"): r_command = r_command + '-n '
    if options.get("nokey"):   r_command = r_command + '-o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null" '
    else:                      r_command = r_command + '-o "StrictHostKeyChecking=no" '
    if options.get("useterm") and not options.get("async"): r_command = r_command + '-t '
    return r_command + host + " " + command

  def execute(command):
    fin, fout = os.popen4(command)
    fin.close()
    data = fout.read()
    fout.close()
    try:    os.waitpid(0, os.WNOHANG)
    except: pass
    return data

  def host_execute(host, command):
    host = "%-20s: " % shorthost(host)
    try:
      fin, fout = os.popen4(command)
      fin.close()
      line = fout.readline()
      while line:
        output((host, COLOR), line.strip().split("\r")[-1], None)
        line = fout.readline()
    except KeyInt:
      output((host, COLOR), ("User Canceled!", YELLOW), None)
      raise
    except:
      output((host, COLOR), ("Failed!", RED), None)

    try:    os.waitpid(0, os.WNOHANG)
    except: pass

  def prepare_args(host, argv):
    arglist = [ ]
    if len(argv) > 1:
      for arg in argv:
        arg = arg.replace('"', '\\"')
        if arg.find(" ") != -1:   arglist.append('"%s"' % arg)
        elif arg.find(";") != -1: arglist.append('"%s"' % arg)
        else:                     arglist.append(arg)
    else:				     
      arglist = argv

    return " ".join(map(lambda x: x.replace("%HOST%", host), arglist))
else:
  def run_scp(options, hostlist, argv): 
    prog, command_list = 'copy ', [ ]
   
    for host in hostlist:
      command  = prog + prepare_args(host, argv)
      title    = highlight(prog, CYAN) + prepare_args(highlight(host, COLOR), argv)
      command_list.append( ("localhost", title, command) )
   
    run_commands(options, command_list)  

  def get_ssh_command(options, host, command):
    r_command = "psexec.exe " 
    host = "\\\\" + host
    return r_command + host + " " + command
    
  def execute(command):
    fin, fout = os.popen4(command)
    fin.close()
    data = fout.read()
    fout.close()
    try:    os.waitpid(0, os.WNOHANG)
    except: pass
    return data
  
  def host_execute(host, command):
    host = "%-20s: " % shorthost(host)
    try:
      fin, fout = os.popen4(command)
      fin.close()
      line = fout.readline()
      while line:
        output((host, COLOR), line.strip().split("\r")[-1], None)
        line = fout.readline()
    except KeyInt:
      output((host, COLOR), ("User Canceled!", YELLOW), None)
      raise
    except:
      output((host, COLOR), ("Failed!", RED), None)
  
    try:    os.waitpid(0, os.WNOHANG)
    except: pass
    
  #MEXEC DIRECT -- Create symlinks used by mexec
  #No symlink in windows need to actually copy files unless there is another way.
  def build_links(mexec, etcpath, binpath):
    output( ("*** Building mexec Symlinks ***", WHITE, True), None)     
    binary = os.path.join(binpath, mexec)
    #Create paths needed
    for path in [ binpath, os.path.join(binpath, "_linked_") ]:
      try:    os.makedirs(path)
      except: pass
  
    #Remove current links
    for link in map(lambda x: os.path.join(path, x), os.listdir(path)):
      if os.path.islink(link):
        os.unlink(link)
  
    #Link host-group actions
    prefixes = [ x[:-12] for x in os.listdir( etcpath ) if x.endswith("_cluster.txt") ]
    for prefix in prefixes:
      for action in HOST_ACTIONS:
        shutil.copyfile(binary, os.path.join(binpath, "_linked_", "%s_%s" % (prefix, action)))
  
    #Link global actions
    for prefix in filter(lambda x: x.find("_") == -1, prefixes):
      for action in CLUSTER_ACTIONS:
        shutil.copyfile(binary, os.path.join(binpath, "_linked_", "%s_%s" % (prefix, action)))
  

     
  def prepare_args(host, argv):
    arglist = [ ]
    if len(argv) > 1:
      for arg in argv:
        #arg = arg.replace('"', '\\"')
        if not arg.endswith("\\"):
           arg = arg.replace("\\\\", "\\")
        if arg.find(" ") != -1:   arglist.append('"%s"' % arg)
        elif arg.find(";") != -1: arglist.append('"%s"' % arg)
        else:                     arglist.append(arg)
    else:				     
      arglist = argv
    #Replace "%HOST%:" with UNC hostname  
    arglist = map(lambda x: x.replace("%HOST%:", win_host(host)), arglist)
    #Replace any other occurrences of hostname with normal host_name
    return " ".join(map(lambda x: x.replace("%HOST%", host), arglist))




if __name__ == "__main__":
  try:
    main()
  except KeyboardInterrupt:
    raise SystemExit
  except SystemExit:
    raise
  except:
    output( ("*** Execution Failed ***", RED), None, None )
    raise

