#!/usr/bin/env cobra
import os, re, sys, popen2, signal, libxml2, getopt, threading, urllib, socket, time
from urllib import quote_plus as quote
from pylib import Logger
from pylib.medusa import asyncore, http_server, cs_pyxmlrpc_handler

#Some globals
FASTSEARCH        = os.environ.get("FASTSEARCH", ".")
FASTSEARCH_BIN    = os.path.join(FASTSEARCH, "bin")
NODE_CONF_FILE    = os.path.join(FASTSEARCH, "etc", "NodeConf.xml")
ESP_VERSION_FILE  = os.path.join(FASTSEARCH, "etc", "VERSION.xml")
VERSION_INFO_FILE = os.path.join(FASTSEARCH, "etc", "ESPDEPLOY_VERSION.xml")
HOTFIX_PATH       = os.path.join(FASTSEARCH, "hotfixes")

#Execute a binary
def execute(path, argv):
  try:
    p = popen2.Popen4("%s %s" % (path, " ".join(argv)))
    data = p.fromchild.read()
    p.wait()
    return data.strip()
  except:
    raise
    log(log.FLOG_WARNING, "Failed to run: %s %s" % (path, " ".join(argv)))
    return "" #failure

#OS/Hardware Specific Information
class SystemPlatform(dict):
  def __init__(self):
    self['platform'] = sys.platform
    self['kernel']   = execute("uname", ["-r"])
    self['hostname'] = socket.getfqdn()
    if sys.platform.find("linux") != -1:
      #TODO: get cpu-count and cpu-speed from /proc/cpuinfo
      #TODO: get mem-total from /proc/meminfo
      pass

  def __str__(self):
    return "%s %s" % (self['platform'], self['kernel'])

#ESP/DataSearch/InStream Specific Information
class ProductPlatform(dict):
  def __init__(self):
    try:
      doc = libxml2.parseFile(ESP_VERSION_FILE)
      context = doc.xpathNewContext()
      for node in context.xpathEval("/version/@*"): self[ node.get_name() ] = node.getContent()
      context.xpathFreeContext()
      doc.freeDoc()
    except:
      raise Exception("version configuration file missing or invalid '%s'" % ESP_VERSION_FILE)

    #Get baseport
    curdir = os.getcwd()
    try:
      os.chdir( os.path.join(FASTSEARCH, "etc", "dtd") )
      fp = file(NODE_CONF_FILE, "rb")
      data = fp.read()
      fp.close()
      doc = libxml2.parseMemory(data, len(data))
      context = doc.xpathNewContext()
      for node in context.xpathEval("/nodes/node/global/portrange/@base"): self['base-port'] = node.getContent()
      context.xpathFreeContext()
      doc.freeDoc()
    except:
      raise Exception("version configuration file missing or invalid '%s'" % NODE_CONF_FILE)
    os.chdir(curdir)

  def __str__(self):
    return "%s %s" % (self['product-name'], self['product-version'])

#Implementation Specific Information
class ProductVersion(dict):
  def __init__(self):
    self.upload_hosts = [ ]
    self.mount_points = [ ] #mount points for disk usage logging
    try:
      doc = libxml2.parseFile(VERSION_INFO_FILE)
      context = doc.xpathNewContext()
      for node in context.xpathEval("/version/@*"): self[ node.get_name() ] = node.getContent()
      self.upload_hosts = map(lambda x: x.getContent(), context.xpathEval("/version/upload-host"))
      self.mount_points = map(lambda x: x.getContent(), context.xpathEval("/version/disk-usage/mount-point"))
      context.xpathFreeContext()
      doc.freeDoc()
    except:
      raise Exception("version configuration file missing or invalid '%s'" % VERSION_INFO_FILE)

  def make_param_list(self, name, data):
    return map(lambda x: ("%s[%s]" % (name, x[0]), "%s" % x[1]), data)

  def upload(self, system, platform, modules, hotfixes):
    #build our getpath
    params = [ ("action", "upload") ]
    mods   = map(lambda x: (x, modules[x]), filter(modules.has_key, modules.order))
    params.extend( self.make_param_list("system",     system.items()) )
    params.extend( self.make_param_list("platform",   platform.items()) )
    params.extend( self.make_param_list("deployment", self.items()) )
    params.extend( self.make_param_list("modules",    mods) )
    params.extend( map(lambda x: ("hotfixes[]", x), hotfixes) )
    getpath = "/version/index.php?" + "&".join( map(lambda x: "%s=%s" % (quote(x[0]), quote(x[1])), params) )

    hosts = self.upload_hosts[:]
    proxy = os.environ.get("BOS3_PROXY", None)
    while len(hosts):
      failed = [ ]
      for path, url in map(lambda x: (x, ("%s%s" % (x, getpath))), hosts):
        if proxy and path.find(".bos3.") != -1: proxies = { "http" : proxy }
        else:                                   proxies = None

        try:
          log(log.FLOG_STATUS, "Uploading Version Information To %s..." % path)
          fp = urllib.urlopen(url, None, proxies)
          for line in fp: log(log.FLOG_INFO, line.rstrip())
          fp.close()
        except Exception, e:
          log_exc(log.FLOG_ERROR, "Failed to upload version information")
          failed.append( path )

      hosts = failed
      if len(hosts):
        log(log.FLOG_ERROR, "Failed to upload version info to all hosts. Will retry in 30 seconds.")
        time.sleep(30)

  def __str__(self):
    return "%s %s - %s-%s (%s)" % (self['product'], self['release'], self['environment'], self['cluster'], self['build-date'])

#Index that contains ESP/FDS Module Version Information
class ModuleIndex(dict):
  MODULES = [ ("RTS Indexer", "frtsobj",    ["-v"], "version\s+([^-]+)"),
              ("RTS Indexer", "indexer",    ["-v"], "indexer\s+([^\s]+)"),
              ("FIXMLindex",  "fixmlindex", ["-V"], "fixmlindex\s+([^s]+)"),
              ("Unity",       "unity",      ["-v"], "Unity\s+Version\s+([^\s]+)"),
              ("QR Server",   "qrserver",   ["-V"], "qrserver\s+([^\s]+)"),
              ("FDispatch",   "fdispatch",  ["-V"], "^([^\s]+)"),
              ("FSearch",     "fsearch",    ["-V"], "^([^\s]+)"),
              ("Caster",      "caster",     ["-V"], "Version:\s+V_(\d+_\d+_\d+)_RELEASE"),
              ("Catcher",     "catcher",    ["-V"], "Version:\s+V_(\d+_\d+_\d+)_RELEASE"),
              ("Clarity",     "clarity",    ["-v"], "Clarity\s+Version\s+([^\s]+)"),
              ("Fidelity",    "fidelity",   ["-v"], "Fidelity\s+Version\s+([^\s]+)"),
              ("Integrity",   "integrity",  ["-v"], "Integrity\s+Version\s+([^\s]+)"),
              ("Dist Link",   "dist-link",  ["-v"], "DistLink\s+Version\s+([^\s]+)"), ]

  def __init__(self):
    self.order = [ ]
    
    for name, binary, argv, regex in self.MODULES:
      path = os.path.join(FASTSEARCH, "bin", binary)
      if os.access(path, os.X_OK):
        match = re.search(regex, execute(path, argv))
        if match:
          self[name] = match.group(1).replace("_", ".")
          self.order.append(name)

    #Get versions of python modules
    try:
      import pydataset
      self['pydataset'] = PYDATASET_VERSION_LONG
      self.order.append("pydataset")
    except:
      pass

#list of hotfixes deployed via espdeploy
class HotfixIndex(list):
  def __init__(self):
    try:
      files   = filter(lambda x: x.endswith("tar.gz") or x.endswith("zip"), os.listdir(HOTFIX_PATH))
      self[:] = map(lambda x: x.split(".")[4], files)
      self.sort()
    except:
      pass

  def __str__(self):
    if len(self): return ", ".join( self )
    else:         return "None"

class DiskUsage(dict):
  def __init__(self, mount_points):
    self.lock    = threading.Lock()
    self.event   = threading.Event()
    self.thread  = None
    self.running = False
    self.mounts  = mount_points
    self.outdir  = os.path.join(FASTSEARCH, "var", "log", "diskusage")
    try:    os.makedirs(self.outdir)
    except: pass

  def get_usage(self, mount):
    try:
      fs, tot, used, avail, percent, mp = execute("df", ['-P','"%s"' % mount]).split("\n")[1].strip().split(None, 5)
      return { 'filesystem' : fs, 'total' : int(tot), 'used' : int(used), 'available' : int(avail), 'mount-point' : mp }
    except:
      return None

  #start this thread
  def start(self):
    self.thread = threading.Thread(target = self.run)
    self.join = self.thread.join #have a call to self.wait call thread.wait
    self.thread.start()

  def stop(self):
    log(log.FLOG_STATUS, "Stopping disk usage logging")
    self.running = False
    self.event.set()

  def join(self): pass

  def run(self):
    self.running = True
    while self.running:
      fp = file(os.path.join(self.outdir, time.strftime("usage.%Y%m%d")), "ab")
      for mount in self.mounts:
        logtime = time.strftime("%Y-%m-%d %H:%M:%S")
        usage = self.get_usage(mount)
        if usage:
          self.lock.acquire()
          self[mount] = usage
          self.lock.release()
          fp.write("[%s] %s: %s/%s/%s (used/available/total)\n" % (logtime, mount, usage['used'], usage['available'], usage['total']))

      fp.close()
      self.event.wait( 60 * 30 ) #only log once every 30 minutes
      
  def getStatistics(self):
    self.lock.acquire()
    stats = dict( self )
    self.lock.release()
    return stats

class Interface(cs_pyxmlrpc_handler.cs_pyxmlrpc_handler):
  MODULE_INFO = { 'type' : "ESP Version", 'name' : "espversion", 'version' : "2.0", 'alerts' : [ ] }

  def __init__(self, port, upload = False):
    cs_pyxmlrpc_handler.cs_pyxmlrpc_handler.__init__(self, port)
    try:
      self.configserver = None
      self._cs_register(self.MODULE_INFO)
    except:
      log_exc(log.FLOG_WARNING, "Failed to register with config server.")

    #Setup signal handling
    if sys.platform == 'win32':
        import pywin32lib
        pywin32lib.SetSignalHandler(self._shutdown_win32)
    else:
        import signal
        signal.signal(signal.SIGTERM, self._shutdown)
        signal.signal(signal.SIGINT,  self._shutdown)

    #load up start-up information we need
    log(log.FLOG_STATUS, "Loading System Platform Information")
    self.system = SystemPlatform()
    log(log.FLOG_STATUS, "Loading ESP Platform Information")
    self.platform = ProductPlatform()
    log(log.FLOG_STATUS, "Loading Deployment Information")
    self.deploy = ProductVersion()
    log(log.FLOG_STATUS, "Loading Deployed Hotfixes")
    self.hotfixes = HotfixIndex()
    log(log.FLOG_STATUS, "Loading Module Version Index")
    self.modules = ModuleIndex()

    if upload:
      self.deploy.upload(self.system, self.platform, self.modules, self.hotfixes)

    #initialize system statistic engines
    self.engines = { }
    if self.deploy.mount_points: self.engines['disk-usage'] = DiskUsage(self.deploy.mount_points)

    #start up all statitic engines
    for name, engine in self.engines.items():
      log(log.FLOG_STATUS, "Initializing statistic engine '%s'" % name)
      engine.start()

    #initialize the server
    log(log.FLOG_STATUS, "Initializing server on port %d" % port)
    self.server = http_server.http_server("", port)
    self.server.install_handler( self )

  def _shutdown_win32(self, sig = None):
    self._shutdown()
    return 1 #or windows will mess with us

  def _shutdown(self, sig = None, frame = None):
    log(log.FLOG_STATUS, "Shutting down...")
    asyncore.shutdown()

    #shutdown all statistics engines
    for engine in self.engines.values(): engine.stop()
    for engine in self.engines.values(): engine.join()

  def GetVersionInfo(self):
    return { 'system'     : dict(self.system),
             'platform'   : dict(self.platform),
             'deployment' : dict(self.deploy), 
             'modules'    : dict(self.modules),
             'hotfixes'   : list(self.hotfixes) }

  def GetStatistics(self):
    output = { }
    for name, stage in self.engines.items():
      output[name] = stage.getStatistics()
    return output


#Program usage information
def usage(error = None):
  print "Usage: %s [options]" % os.path.basename(sys.argv[0])
  print ""
  print "This program will display/upload version/system information for a cluster"
  print ""
  print "Options:"
  print "  -h, --help            Show this information"
  print "  --upload              Upload version information to admin-gui(s)"
  print "  --daemon              Run as a daemon. Use this if running in node controller"
  print "  --port=PORT           Run daemon on this port. (Default: 18000)"
  print ""
  print "Config: %s" % VERSION_INFO_FILE
  print "  This config file will specify the hosts to upload to,"
  print " as well as any logging/statistic gathering that is to be"
  print " run periodically (ie gather OS specific stats)"
  print ""
  if error:
    print "ERROR: %s" % error
    print ""
  raise SystemExit(1)

if __name__ == "__main__":
  upload, daemon, port = False, False, 18000

  try: opts, args = getopt.getopt(sys.argv[1:], "h", [ "help", "daemon", "port=", "upload" ])
  except Exception, e: usage("Command Line Error: %s" % e)

  for o, a in opts:
    if o in ("-h", "--help"): usage()
    if o == "--daemon": daemon = True
    if o == "--upload": upload = True
    if o == "--port":
      try:    port = int(a)
      except: usage("--port must be supplied an integer")

  log.init("espversion")

  if daemon:
    server = Interface(port, upload)
    asyncore.loop()
  else:
    #display version information to stdout
    system   = SystemPlatform()
    platform = ProductPlatform()
    deploy   = ProductVersion()
    hotfixes = HotfixIndex()
    modules  = ModuleIndex()

    if upload: deploy.upload(system, platform, modules, hotfixes)

    print deploy
    print "Platform: %s (%s)" % (platform, system)
    print "Hotfixes: %s" % hotfixes
    print ""
    print "Module Versions:"
    maxsize = max(map(len, modules.keys()))
    mformat = "%%%ds: %%s" % (maxsize + 2)
    for module in modules.order:
      print mformat % (module, modules[module])
    print ""

