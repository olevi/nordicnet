<?php
/*************** Initialize Defaults **************/
$CLUSTER_ORDER   = Array(); //Loaded from config
$ENVIRON_ORDER   = Array(); //Loaded from config
$CLARITY_LINKS   = Array(); //Loaded from config
$PRIMARY_MODULES = Array("RTS Indexer", "QR Server", "Unity", "Caster", "Clarity", "Integrity", "Fidelity", "Dist Link");
$MODULES         = Array(); //Array of modulename => show on simple view (populated automatically)

/**************** Load Configuration ***************/
$ATTRS = Array(); $CHARDATA = "";
function xml_start_element($parser, $name, $attrs) { global $CHARDATA, $ATTRS; $CHARDATA = ""; $ATTRS = $attrs; }
function xml_character_data($parser, $data)        { global $CHARDATA; $CHARDATA .= $data; }

function xml_end_element($parser, $name) {
  global $CLARITY_LINKS, $CLUSTER_ORDER, $ENVIRON_ORDER, $CHARDATA, $ATTRS;
  switch($name) {
  case "CLARITY-URL":       $CLARITY_LINKS[] = sprintf("<a target='_new' href='%s'>%s</a>", trim($CHARDATA), trim($ATTRS['NAME'])); break;
  case "ENVIRONMENT-ORDER": $ENVIRON_ORDER   = array_flip( explode(" ", $CHARDATA) ); break; 
  case "CLUSTER-ORDER":     $CLUSTER_ORDER   = array_flip( explode(" ", $CHARDATA) ); break; 
  }
}

$xml_parser = xml_parser_create();
xml_set_element_handler($xml_parser, "xml_start_element", "xml_end_element");
xml_set_character_data_handler($xml_parser, "xml_character_data");
if ($fp = @fopen("$DOCUMENT_ROOT/../etc/ESPDEPLOY_VERSION.xml", "rb")) {
  while ($data = fread($fp, 4096)) xml_parse($xml_parser, $data, feof($fp));
  @fclose($fp);
}

/**************** Main Program Starts Now ****************/
$view     = strlen($_GET['view']) ? $_GET['view'] : "normal";
$extended = $_GET['mode'] == "extended";

/* Get a handle on the directory for storing info at */
$VARPATH = realpath($_SERVER['DOCUMENT_ROOT'] . "/../var");
$VERPATH = $VARPATH . "/espversion";
if (!file_exists($VERPATH)) mkdir($VERPATH, 0700);

/*********** Simple Support Methods **********/
function fwriteArray($fp, $array, $keys = true) {
  fwrite($fp, "Array( ");
  if (is_array($array)) {
    if ($keys) { foreach ($array as $key => $value) fwrite($fp, "'$key' => '$value', "); }
    else       { foreach ($array as $value) fwrite($fp, "'$value', ");                   }
  }
  fwrite($fp, ")");
}

function array_get($array, $key, $default = null) {
  return array_key_exists($key, $array) ? $array[$key] : $default;
}

function hostname($host) {
  return array_shift( explode(".", $host) );
}

function baseurl($host) {
  return sprintf("http://%s:%d", $host['system']['hostname'], $host['platform']['base-port'] + 3000);
}

function adminurl($host) {
  return sprintf("<a target='_new' href='%s/'>%s</a>", baseurl($host), hostname($host['system']['hostname']));
}

function clustername($host) {
  return implode("-", array_filter( Array($host['deployment']['environment'], $host['deployment']['cluster']), "count" ));
}

function indexurl($host) {
  return sprintf("<a target='_new' href='%s/version/'>%s</a>", baseurl($host), clustername($host));
}

/************ Run simple actions that don't require host index to be loaded **********/
if ($_GET['action'] == "upload") {
  /* Upload Version For A Host */
  $hostname = $_GET['system']['hostname'];
  $fp = fopen("$VERPATH/$hostname", "wb");
  fwrite($fp, "<?php\n\n");
  fwrite($fp, "\$hosts[] = Array(\n");
  fwrite($fp, "    'system'     => "); fwriteArray($fp, $_GET['system']);          fwrite($fp, ",\n");
  fwrite($fp, "    'platform'   => "); fwriteArray($fp, $_GET['platform']);        fwrite($fp, ",\n");
  fwrite($fp, "    'deployment' => "); fwriteArray($fp, $_GET['deployment']);      fwrite($fp, ",\n");
  fwrite($fp, "    'modules'    => "); fwriteArray($fp, $_GET['modules']);         fwrite($fp, ",\n");
  fwrite($fp, "    'hotfixes'   => "); fwriteArray($fp, $_GET['hotfixes'], false); fwrite($fp, ",\n");
  fwrite($fp, "  );\n");
  fwrite($fp, "\n?>\n");
  fclose($fp);
  print "Uploaded Succeeded!\n";
  exit;
} elseif ($_GET['action'] == "delete") {
  /* Delete Information For A Host */
  $filename = $VERPATH . "/" . $_GET['host'];
  if (file_exists($filename)) unlink($filename);
  header("Location: " . $_SERVER['SCRIPT_NAME']);
  exit;
}

/*********** Load up and index Host Information ************/
$hosts = Array();
$d = opendir($VERPATH);
while (false !== ($file = readdir($d))) {
  if (is_file($VERPATH . "/" . $file)) include $VERPATH . "/" . $file;
}
closedir($d);

$exhosts = Array();
foreach ($hosts as $host) {
  foreach ($host['modules'] as $module => $version) $MODULES[$module] = in_array($module, $PRIMARY_MODULES);
  $PRODUCT_NAME = $host['deployment']['product'];
  $envsort = sprintf("%010d", array_get($ENVIRON_ORDER, $host['deployment']['environment'], 9999999999));
  $clusort = sprintf("%010d", array_get($CLUSTER_ORDER, $host['deployment']['cluster'],     9999999999));
  $exhosts["$envsort-$clusort-" . $host['system']['hostname']] = $host;
}
ksort($exhosts);

/************** Run simple actions that require host index loaded *************/
if ($_GET['action'] == "select") {
  foreach ($hosts as $host) {
    if ($host['system']['hostname'] == $_GET['host']) {
      header("Location: " . baseurl($host));
      exit;
    }
  }
  //Just in case host isn't in index
  header("Location: http://" . $_GET['host'] . ":16000/");
  exit;
}

/********** GUI Support Methods **********/
function PrintHeader() {
  global $PRODUCT_NAME;
  print "<html>\n";
  print "<head>\n";
  print "  <title>$PRODUCT_NAME Cluster Index</title>\n";
  print "  <style type='text/css'>\n";
  print "  <!--\n";
  print "  body { background-color: #FFFFFF; color: #330066; font-family: Tahoma,sans-serif; font-size: 12px; }\n";
  print "  table { text-align: left; color: #330066; padding: 0px; border: 0px; font-size: 12px; font-family: Tahoma,sans-serif; }\n";
  print "  table { border-top: 1px solid #316CA2; border-left: 1px solid #336699; }";
  print "  table.gtop { color: #330066; background-color: #EEEEEE; padding: 0px; font-size: 12px; font-family: Tahoma,sans-serif; border: 1px solid #336699; }\n";
  print "  .ghead { border-right: 1px solid #316CA2; border-bottom: 1px solid #316CA2; padding: 0px 8px; font-weight: bold; white-space: nowrap; }\n";
  print "  .hrow { font-size: 10px; text-align: center; vertical-align: bottom; background-color: #CCE1EB; }\n";
  print "  .grow1 { background-color: #EBF4F8; text-align: left; vertical-align: middle; }\n";
  print "  .grow2 { background-color: #D8EBF3; text-align: left; vertical-align: middle; }\n";
  print "  .gcell { padding: 0px 5px; border-right: 1px solid #316CA2; border-bottom: 1px solid #316CA2; white-space: nowrap; }\n";
  print "  .gcellt { padding: 0px 5px; border-right: 1px solid #316CA2; border-top: 3px solid #316CA2; border-bottom: 1px solid #316CA2; white-space: nowrap; }\n";
  print "  a { text-decoration: none; }\n";
  print "  a:link { color: #0066a1; }\n";
  print "  a:visited { color: #0066a1; }\n";
  print "  a:hover { color: #FF7700; }\n";
  print "  -->\n";
  print "  </style>\n";
  print "  ";
  print "</head>\n";
  print "<body>\n";
}

function PrintFooter() {
  print "</body>\n</html>\n";
}

function TableHeader() {
  global $PRODUCT_NAME, $CLARITY_LINKS, $extended, $view;
  PrintHeader();
  $omode = ($extended ? "simple" : "extended");
  $links = Array( "<a target='_new' href='/acuity/'>testing</a>" );
  if ($view == "hotfixes") $links[] = "<a href='?'>versions</a>";
  else                     $links[] = "<a href='?view=hotfixes'>hotfixes</a>";
  $links[] = "<a href='?mode=$omode'>$omode&nbsp;mode</a>";

  print "<!-- Start Header Table For Indexing Overview -->\n";
  print "<table align='center' class='gtop' style='width:auto;padding:0px' cellpadding='0' cellspacing='0' border='0'>\n";
  print "<tr class='hrow' style='padding:0px;border:0px'><td class='ghead' style='width:100%;border-right:0px;font-size:18px;'>\n";

  print "<table width='100%' cellpadding='0' cellspacing='0' border='0' style='border:0px'><tr>\n";
  if (count($CLARITY_LINKS)) {
    print "<td align='left' style='font-size:11px;font-weight:normal' width='25%'><b>Clarity:</b>&nbsp;&nbsp;" . implode("&nbsp;|&nbsp;", $CLARITY_LINKS) . "</td>\n";
  } else {
    print "<td align='left' style='font-size:11px;font-weight:normal' width='25%'>&nbsp;</td>\n";
  }
  print "<td align='center' width='50%' style='font-size:16px'><b>$PRODUCT_NAME Cluster Index</b></td>\n";
  print "<td width='25%' align='right' style='font-size:10px;font-weight:normal'>\n";
  print "<img src='/admin/graphics/prikker.gif'/>&nbsp;&nbsp;" . implode("&nbsp;|&nbsp;", $links) . "</td>\n";
  print "</tr></table>\n";

  print "</td></tr>\n";
  print "<tr><td style='padding:5px'>\n";
}

function TableFooter() {
  print "</td></tr></table>\n";
  PrintFooter();
}

/********************************\
|     Render The Display         |
\********************************/

if ($_GET['action'] == "export") {
  /* Render The CSV File */

} elseif ($view == "navbar") {
  PrintHeader();

  /* Find the selected System */
  $myhost = strlen($_GET['host']) ? $_GET['host'] : $_SERVER['SERVER_NAME'];
  $selected = null;
  foreach ($exhosts as $host) {
    if ($myhost == $host['system']['hostname']) $selected = clustername($host);
  }
  $display_sel = ($selected != null) ? "<b>Selected:</b>&nbsp;&nbsp;$selected" : "&nbsp;";

  print "<form style='padding:0px;margin:0px' name='selector' target='_top'>\n";
  print "<table align='center' class='gtop' style='border-bottom:0px;width:1000px;padding:0px' cellpadding='0' cellspacing='0' border='0'>\n";
  print "<tr class='hrow' style='padding:0px;border:0px'><td class='ghead' style='width:100%;border-right:0px;font-size:18px;'>\n";
  print "<table width='100%' cellpadding='0' cellspacing='0' border='0' style='border:0px'><tr>\n";
  print "<td align='left' style='font-size:11px;font-weight:normal' width='25%'>$display_sel</td>\n";
  print "<td align='center' style='vertical-align:middle' width='50%' style='font-size:14px'>\n";
  print "  <b>Cluster:&nbsp;&nbsp;</b>\n";
  print "   <input type='hidden' name='action' value='select'/>\n";
  print "   <select name='host' style='font-family:courier; font-size: 12px; width:250px' onChange='submit()'>\n";
  print "     <option value='" . $_SERVER['SERVER_NAME'] . "'>$PRODUCT_NAME Cluster Index</option>\n";
  foreach ($exhosts as $host) {
    $hostname = $host['system']['hostname'];
    $cluster  = clustername($host);

    if ($cluster == $selected) {
      print "   <option value='$hostname' selected>$cluster - " . hostname($hostname) . "</option>\n";
    } else {
      print "   <option value='$hostname'>$cluster - " . hostname($hostname) . "</option>\n";
    }
  }
  print "   </select>\n";

  print "</td>\n";
  print "<td width='25%' align='right' style='font-size:10px;font-weight:normal'>\n";
  print count($CLARITY_LINKS) ? "<b>Clarity:</b>&nbsp;&nbsp;" . implode(" | ", $CLARITY_LINKS) . "</td>\n" : "&nbsp;";
  print "</tr></table>\n";

  print "</td></tr></table>\n";
  print "</form>\n";

  PrintFooter();
} elseif ($view == "hotfixes") {
  TableHeader();
  print "<table cellpadding='0' cellspacing='0' border='0' style='width:1000px' align='left'>\n";
  print "<tr class='hrow'>\n";
  print "  <th class='ghead'>Environment</th>\n";
  print "  <th class='ghead'>Release</th>\n";
  print "  <th class='ghead'>Admin Host</th>\n";
  print "  <th class='ghead'>Product</th>\n";
  print "  <th class='ghead'>Hotfixes</th>\n";
  print "</tr>\n";

  $lastenv == null;
  $row = 1;
  foreach ($exhosts as $host) {
    $row     = ($row % 2) + 1;
    $curenv  = $host['deployment']['environment'];
    $gcell   = (($lastenv == null) || ($lastenv == $curenv)) ? $class="gcell" : "gcellt";
    $lastenv = $curenv;

    print "<tr class='grow$row'>\n";
    print "  <td class='$gcell' style='text-align:left'>" . indexurl($host) . "</td>\n";
    print "  <td class='$gcell'>" . $host['deployment']['release'] . "</td>\n";
    print "  <td class='$gcell' style='text-align:left'>" . adminurl($host) . "</td>\n";
    print "  <td class='$gcell' style='text-align:left'>" . $host['platform']['product-name'] . " " . $host['platform']['product-version'] . "</td>\n";
    if (count($host['hotfixes'])) print "  <td class='$gcell'>" . implode(", ", $host['hotfixes']) . "</td>\n";
    else                          print "  <td class='$gcell'>None</td>\n";
    print "</tr>\n";
  }

  print "</table>";

  TableFooter();
} else {
  /* Render The Table */
  TableHeader();

  print "<table cellpadding='0' cellspacing='0' border='0' align='left'>\n";
  print "<tr class='hrow'>\n";
  print "  <th class='ghead'>Environment</th>\n";
  print "  <th class='ghead'>Release</th>\n";
  print "  <th class='ghead'>Admin Host</th>\n";
  print "  <th class='ghead'>Kernel</th>\n";
  print "  <th class='ghead'>Product</th>\n";

  foreach ($MODULES as $module => $ext) {
    if ($extended || $ext) print "  <th class='ghead'>$module</th>\n";
  }
  if ($extended) print "  <th class='ghead'>Actions</th>\n";
  print "</tr>\n";

  /* Display the hosts */
  $lastenv == null;
  $row = 1;
  foreach ($exhosts as $host) {
    $row     = ($row % 2) + 1;
    $curenv  = $host['deployment']['environment'];
    $gcell   = (($lastenv == null) || ($lastenv == $curenv)) ? $class="gcell" : "gcellt";
    $lastenv = $curenv;

    print "<tr class='grow$row'>\n";
    print "  <td class='$gcell' style='text-align:left'>" . indexurl($host) . "</td>\n";
    print "  <td class='$gcell'>" . $host['deployment']['release'] . "</td>\n";
    print "  <td class='$gcell' style='text-align:left'>" . adminurl($host) . "</td>\n";
    print "  <td class='$gcell' style='text-align:left'>" . $host['system']['kernel'] . "</td>\n";
    print "  <td class='$gcell' style='text-align:left'>" . $host['platform']['product-name'] . " " . $host['platform']['product-version'] . "</td>\n";

    foreach ($MODULES as $module => $ext) {
      if ($extended || $ext) print "  <td class='$gcell'>" . array_get($host['modules'], $module, "N/A") . "</td>\n";
    }
    if ($extended) print "  <td class='$gcell'>[ <a href='?action=delete&host=" . $host['system']['hostname'] . "'>delete</a> ]</td>\n";
    print "</tr>\n";
  }

  print "</table>\n";

  /* Footer */
  TableFooter();
}

?>
