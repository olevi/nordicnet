@echo off

rem Choose target
echo NordicNet deployment script
echo ---------------------------
echo Choose target environment
echo.
echo 1. DEV   - fastdev.ekolitt.local
echo 2. CRAWL - nnpcrawl.ekolitt.local
echo 3. PROD  - nnphbwapp001.nordicnet.net
echo.
echo.
set /p index=Choose target host [1,2,3]: 

if %index%==1 (set INSTALL_PROFILE=install-profile-DEV.xml)
if %index%==2 (set INSTALL_PROFILE=install-profile-CRAWL.xml)
if %index%==3 (set INSTALL_PROFILE=install-profile-PROD.xml)

rem Check for Java
if not "%JAVA_HOME%" == "" goto okJava
echo.
echo The JAVA_HOME environment variable is not defined correctly
echo This environment variable is needed to run this program
goto end
:okJava

rem General parameters
set ESPDEPLOY_HOME=E:\_deploy\_tools\espdeploy
set PATH=%PATH%;%ESPDEPLOY_HOME%\bin

rem Deployment parameters
set HOST_DEPLOY_DIR=E:\_deploy
set ESP_INSTALL_DIR=E:\esp