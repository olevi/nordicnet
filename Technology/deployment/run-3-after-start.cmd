@echo off
call run-include.cmd

rem =========================================

chdir %ESP_INSTALL_DIR%\bin
call setupenv.cmd
chdir %HOST_DEPLOY_DIR%

pause

rem Removes crawler (and related services) on nodes with the service-set "noncrawler"
call espdeploy --deploy -c deploy-configurations/disable-crawler.xml %INSTALL_PROFILE% 

pause

rem Add procservers, 4 in total
nctrl add procserver
nctrl add procserver
nctrl add procserver

pause

rem Add collection, pipeline comes from overlay
call collection-admin -m addcollection -n companies -p Companies -c webcluster

pause

rem Import the search profiles
for %%G in (search-profiles/*.zip) do importsearchprofile -f search-profiles/%%G

pause

rem =========================================
:end

pause