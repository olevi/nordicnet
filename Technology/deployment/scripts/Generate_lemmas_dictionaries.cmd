@echo off
echo LEMMAS (query side) for language %1
echo -----------------------------
echo Extracting data from database..
java -jar SQLster.jar %DB_CONN% %DB_USER% %DB_PASS% "select CONTENT from FAST_PRODUCTS_LEMMAS where langcode=upper('%1') order by content asc" > dictionaries_temp\text\lemmas_%1.txt

echo.
echo Compiling dictionary...
set var=%1
set LANGCODE=%var:~0,2%
call dictcompile -o dictionaries_temp\lemmatization\%LANGCODE%_NA_exp.aut dictionaries_temp\lemmatization\%LANGCODE%_NA_red.aut -t LD -i dictionaries_temp\text\lemmas_%1.txt -e iso-8859-1
echo.