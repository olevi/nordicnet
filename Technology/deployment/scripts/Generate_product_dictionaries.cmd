@echo off
echo PRODUCT ID dictionary
echo -----------------------------
echo Extracting data from database..
java -jar SQLster.jar %DB_CONN% %DB_USER% %DB_PASS% "select lower(term) || '.' || productno from product_inflections where LANG = upper('%1') ORDER BY term" > dictionaries_temp\text\productdata_%1.txt
echo.
echo Compiling dictionary...
call dictcompile -o dictionaries_temp\matching\nnet_products_%1.aut -t ED -i dictionaries_temp\text\productdata_%1.txt -d "\." -e ISO-8859-1
echo.