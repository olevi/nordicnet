@echo off
If {%1}=={} @echo Syntax Call StripBN FileName&goto :EOF
if not exist %1 @echo StripBN %1 not found.&goto :EOF
setlocal
set file=%1
copy %file% %TEMP%\StripBN.tmp
del /q %file%
for /f "Tokens=*" %%i in (%TEMP%\StripBN.tmp) do if {%%i} NEQ {} @echo %%i>>%file%
endlocal