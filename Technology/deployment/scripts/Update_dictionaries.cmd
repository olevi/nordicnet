@echo off

set DB_CONN=__DB_CONN__
set DB_USER=__DB_USER__
set DB_PASS=__DB_PASS__
 
if exist dictionaries_temp. (rmdir dictionaries_temp /S /Q)
mkdir dictionaries_temp\text

FOR %%G IN (sve fin nor dan eng) DO (
echo.
echo.
echo ----------------------------
echo Processing language %%G
echo ----------------------------
echo.
rem Synonyms
call Generate_synonym_dictionaries.cmd %%G
echo.

rem Lemmas
call Generate_lemmas_dictionaries.cmd %%G
echo.

rem Spellcheck
call Generate_spellcheck_dictionaries.cmd %%G
echo.

rem Product ID
call Generate_product_dictionaries.cmd %%G
echo.

rem Autosuggets for 'where'
call Generate_location_dictionaries.cmd %%G
echo.

rem Autosuggets for 'what'
call Generate_autosuggest_what.cmd %%G
echo.
)

rem Product ID
call Generate_propernames_dictionaries.cmd
echo.

rem Stopping all services before distribution
call :Shutdown

rem Distributing dictionaries
call :Distribute

rem Starting up all services
call :Startup

rem Cleaning up temporary directory
call :Clean

goto eof

:Shutdown
echo Stopping all services before distribution
echo -----------------------------------------
echo.
echo Stopping qr server (lemmatization/spellcheck)
call nctrl stop qrserver 

echo Stopping processor servers (synonyms)
call nctrl stop procserver_1
call nctrl stop procserver_2
call nctrl stop procserver_3
call nctrl stop procserver_4

echo Stopping completion server (locations)
call nctrl stop completionserver

echo Stopping query matching server (products)
call nctrl stop querymatchingserver
goto eof

:Distribute
echo Distributing dictionaries
echo -------------------------
echo.
call xcopy dictionaries_temp %FASTSEARCH%\resources\dictionaries\ /Y /E /Q
goto eof

:Startup
echo Starting up all services
echo ------------------------
echo.
echo Starting up completionserver again...
call nctrl start completionserver
echo.
echo Starting up querymatchingserver again...
call nctrl start querymatchingserver
echo.
echo Starting up QR server again...
call nctrl start qrserver
echo.
echo Starting up procservers..
call nctrl start procserver_1
call nctrl start procserver_2
call nctrl start procserver_3
call nctrl start procserver_4
echo.
del E:\esp\adminserver\webapps\adminserver\downloads\resources\dictionaries\*.* /Q /S
echo Deploying all search views..
call view-admin -m deploy -a
echo.
echo Publishing changes for all search profiles...
FOR /F "tokens=2 skip=2 delims= " %%G IN ('searchprofile-admin list') DO (
 echo Search profile: %%G
 call searchprofile-admin -s %%G publish
)
goto eof

:Clean
echo Cleaning up temporary directory
echo -------------------------------
echo.
call rmdir dictionaries_temp /S /Q 
goto eof

:eof
echo Done
rem pause