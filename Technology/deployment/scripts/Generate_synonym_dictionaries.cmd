@echo off
if %1.==. (set 1=SVE)
echo SYNONYMS (index side) for language %1
echo -----------------------------
echo Extracting data from database..
java -jar SQLster.jar %DB_CONN% %DB_USER% %DB_PASS% "select replace(replace(replace(replace(prodnamn,'- ',' '),' -',' '),'-,',','),',-',',') || '.' || synonyms from fast_synonyms_matcher where langcode = upper('%1') order by prodnamn asc" > dictionaries_temp\text\synonyms_ED_%1.txt

echo.
echo Compiling dictionary...
call dictcompile -o dictionaries_temp\synonyms\dp\synonyms_ED_%1.aut -t ED -i dictionaries_temp\text\synonyms_ED_%1.txt -d "\." -e iso-8859-1
echo.