@echo off
REM This script performs index updates from Oracle database and deletes documents from index reported in FASTEXPORT_REMOVE
REM Created: Apr 2008
REM Modified: Jun 2009
REM Author: Marcus Johansson/Pontus Hym�r

REM Suspend indexing during feeding to reduce load
call indexeradmin suspendindexing

REM Restart docprocs to clean out logs
call psctrl stop

set curr=%CD%
E:
cd E:\esp\connectors\jdbc\bin
echo.
echo Feeding index from database...
call connect.bat start -f ..\etc\companies_fetchall.xml
echo Feeding complete
echo.

echo Removing deleted companies from index...
call connect.bat start -f ..\etc\companies_remove.xml
echo Removal complete
echo.

call indexeradmin resumeindexing

cd %curr%

echo Done