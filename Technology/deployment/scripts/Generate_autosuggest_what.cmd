@echo off
echo AUTOSUGGEST WHAT for language %1
echo -----------------------------
echo Extracting data from database..
java -jar SQLster.jar %DB_CONN% %DB_USER% %DB_PASS% "select term from table(fast_pkg.autosuggestion_what(1, '%1'))" > dictionaries_temp\text\nnet_completion_what_%1.txt

echo.
echo Compiling dictionary...
set var=%1
call dictcompile -o dictionaries_temp\matching\nnet_completion_what_%1.aut -t ED -i dictionaries_temp\text\nnet_completion_what_%1.txt -d "###" -e iso-8859-1
echo.