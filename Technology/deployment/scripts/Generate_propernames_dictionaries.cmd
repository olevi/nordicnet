@echo off

echo PROPERNAMES, common for all languages
echo -----------------------------
echo Extracting data from database..
java -jar SQLster.jar %DB_CONN% %DB_USER% %DB_PASS% "select term from table(fast_pkg.propernames(1))" > dictionaries_temp\text\nordicnet_propernames.txt
echo.

echo Compiling dictionary...

rem  Update the file on disk...
call dictcompile -o dictionaries_temp\spellcheck\nordicnet_propernames_iso8859_1.ftaut -t SPP -i dictionaries_temp\text\nordicnet_propernames.txt -d "###" -e ISO-8859-1

rem ...as well as the list in dictman
rem cobra -c "print open('dictionaries_temp/text/nordicnet_propernames.txt', 'r').read().replace('###', '\t')" > dictionaries_temp\text\nordicnet_propernames_tabs.txt
rem call StripBN dictionaries_temp\text\nordicnet_propernames_tabs.txt
rem call dictman -e "truncatedictionary SPP indep_propernames_iso8859_1"
rem call dictman -e "import SPP indep_propernames_iso8859_1 dictionaries_temp\text\nordicnet_propernames_tabs.txt \t ISO-8859-1 o"



