@echo off

set LANGCODE=%1

echo SPELLCHECK (query side) for language %1
echo -----------------------------
echo Extracting data from database..
java -jar SQLster.jar %DB_CONN% %DB_USER% %DB_PASS% "select term from table(fast_pkg.spellcheck(1, '%1'))" > dictionaries_temp\text\spelldata_%1.txt
echo.

echo Compiling %1 dictionary...
set var=%1
set LANGCODE=%var:~0,2%

call dictcompile -o dictionaries_temp\spellcheck\%LANGCODE%_spell_iso8859_1.ftaut -t SPW -i dictionaries_temp\text\spelldata_%1.txt -d "###" -e ISO-8859-1
echo.