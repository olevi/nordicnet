# coding=utf-8

import urllib2, re
from smtplib import SMTP
import datetime


errormessage = ""
successmessage = ""
querystring =  'http://nnpvmwapp002.nordicnet.net:8080/ComperioFrontEndService/httpxml?mycustomattribute=true&what=%s&where=%s'

termstotest = [[("bil", "stockholm"), 151], [("system", "göteborg"), 350], [("dörrar", "malmö"), 28], [("system", "helsingborg"), 58]]
pattern = 'totalHits=\"\d+\"'


def nomatch(terms):
    print 'no match for what/where %s,%s' % terms

def log(s):
    print 'logging ' + s
    f = open('c:\\temp\\indexcontentlogs.txt','a')
    f.writelines(str(datetime.datetime.now()) +"  " +  s + "\n")
    f.close()

def add2ErrorMessage(s):
    global errormessage
    errormessage = errormessage + '\n' + s

def add2SuccessMessage(s):
    global successmessage
    successmessage = successmessage + '\n' + s


def reporterror(terms, number):
    what = terms[0][0]
    where = terms[0][1]
    expected = terms[1]
    s = "Index volume is outside expectations for " + what + "/" + where + ". Expecting " + str(expected) + " items. Got " + str(number) + "."
    log(s)
#    print 'this works'
    add2ErrorMessage(s)
#    print 'error: '  +s
    #send email

def reportsuccess(terms, number):
    what = terms[0][0]
    where = terms[0][1]
    expected = terms[1]
    s = "Index volume is normal for " + what + "/" + where + ". Expecting " + str(expected) + " items. Got " + str(number) + "."
    log(s)
#    print 'this works'
    add2SuccessMessage(s)
#    print 'error: '  +s
    #send email

#pattern = "totalHits......"

log('starting a log')


for terms in termstotest:
#    print terms[0] #CONTAINS (what, where)
    q = querystring % terms[0] #inserting the what/where
#    print q
    res = urllib2.urlopen(q).read()
    #print res[2000:5000]
    m = re.search(pattern, res)
    if m:
       # print "got m"
        resnumber1 = m.group()
        #print terms
        print(resnumber1)
        resnumber = resnumber1[11:-1]
        print resnumber

        expected = terms[1]
       # print 'expected: ' + str(expected) + ' for ' + str(terms[0])
        try:
            int_res = int(resnumber)
            diff = abs(int_res - expected)
            margin = expected*0.2
            if (diff > margin):
                print "number is off by more than 20%, was expecting " + str(expected) + ", but got " + resnumber
                reporterror(terms, resnumber)
                #print 'reported error'
            else:
                reportsuccess(terms, resnumber)
                #print "everything's ok"
        except Exception, err:
            print Exception, err
            nomatch(terms[0])
    else:
        nomatch(terms[0])


if len(errormessage)>20:
    debuglevel = 0

    smtp = SMTP()
    smtp.set_debuglevel(debuglevel)
    smtp.connect('82.99.13.108', 587)
    smtp.ehlo()
    smtp.starttls()
    smtp.login('olekristianvillabo@gmail.com', 'nludvznikkfztcce')
    print 'email logged in'
    from_addr = "FAST ESP monitoring <support@comperiosearch.com>"
    #to_addr = "ole-kristian.villabo@comperiosearch.com; okv@uptimecomperio.no"
    to_addr =  ['support@comperiosearch.com', 'ole-kristian.villabo@uptimecomperio.no']

    subj = "Issues with index content at nnpvmwapp002.nordicnet.net - number of items not as expected."
    date = datetime.datetime.now().strftime( "%d/%m/%Y %H:%M" )

    message_text = "Hello\nThis is a mail from your server: nnpvmwapp002.nordicnet.net \n\n" + errormessage + "\n\n At least one time this happened, it was because the server was restarted in the middle of the feeding process (June 2017), which caused the actual content of specific items to be different than what it needed to be even though the actual items were indexed. Remedy was to restart feeding process.\n\nPlease contact Uptime Comperio at support@comperiosearch.com or +4790880939"

    msg = "From: %s\nTo: %s\nSubject: %s\nDate: %s\n\n%s" % ( from_addr, to_addr, subj, date, message_text )

    smtp.sendmail(from_addr, to_addr, msg)
    smtp.quit()
    print 'email sent to ' + str(to_addr)

else:
    print 'no errors apparently'

