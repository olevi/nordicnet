package se.nordicnet.cfes.search.tasks;

import java.util.ArrayList;
import java.util.List;


import no.comperio.fes.search.SearchContext;
import no.comperio.fes.search.common.result.Hit;
import no.comperio.fes.search.common.result.ResultData;
import no.comperio.fes.search.common.result.ResultPage;
import no.comperio.fes.search.common.result.ResultSegment;
import no.comperio.fes.search.common.result.ResultSet;

import org.junit.Before;
import org.junit.Test;

public class RExtractProductCategoriesTaskTest{

	private RExtractProductCategoriesTask task;
	private SearchContext context;
	private Hit hit;

	public void setUp(String taxonomies) throws Exception {
		
		task = new RExtractProductCategoriesTask();
		
		hit = new Hit();
		hit.addField("producthierarchydan", taxonomies);
		
		List<Hit> hits = new ArrayList<Hit>();
		hits.add(hit);
		
		ResultSet resultSet = new ResultSet();
		resultSet.setHits(hits);
		
		ResultPage resultPage = new ResultPage();
		resultPage.setResultSet(resultSet);
		
		ResultSegment segment = new ResultSegment();
		segment.addResultPage(resultPage);
		
		List<ResultSegment> segments = new ArrayList<ResultSegment>();
		segments.add(segment);
		
		ResultData resultData = new ResultData();
		resultData.setSegments(segments);
		
		context = new SearchContext();
		context.setResultData(resultData);
		
		
	}
	
	@Test
	public void twoCategoriesTest() throws Exception{
		
		setUp("#nordicnet\\Kemikalier til forskellige industrier\\Kemikalier for kemisk industri#Kemi\\Kemikalier til forskellige industrier\\Kemikalier for papirindustrien#nordicnet\\Kemikalier til forskellige industrier\\Kemikalier#Kemi\\Ovrige kemiske produkter, forbindelser\\Fosfater");
		task.executeSearchTask(context);
		List<String> productCategories= task.getProductCategories();
		assert(productCategories.get(0).equals("Kemikalier til forskellige industrier"));
		assert(productCategories.get(1).equals("Øvrige kemiske produkter, forbindelser"));
		assert(productCategories.size()==(2));
	}

	@Test
	public void oneCategoryTest() throws Exception{
		
		setUp("#nordicnet\\Kemikalier til forskellige industrier\\Kemikalier for kemisk industri#Kemi\\Kemikalier til forskellige industrier\\Kemikalier for papirindustrien#nordicnet\\Kemikalier til forskellige industrier\\Kemikalier");
		task.executeSearchTask(context);
		List<String> productCategories= task.getProductCategories();
		assert(productCategories.get(0).equals("Kemikalier til forskellige industrier"));
		assert(productCategories.size()==(1));
	}
	
	@Test
	public void manyCategoriesTest() throws Exception{
		
		setUp("#Kemikalier og kemiske produkter\\Jernforbindelser#Kemikalier og kemiske produkter\\Jernforbindelser\\Jernsulfat#Kemikalier og kemiske produkter\\Kemikalier til forskellige industrier#Kloakvæsen, renovationsvæsen, renholdelse mv.\\Afløbsrensning, affaldshåndtering, renholdelse o.lign.\\Rensningsanlæg#Kloakvæsen, renovationsvæsen, renholdelse mv.\\Afløbsrensning, affaldshåndtering, renholdelse o.lign.#Kemikalier og kemiske produkter\\Aluminium- og ammoniakforbindelser#Kloakvæsen, renovationsvæsen, renholdelse mv.#Kemikalier og kemiske produkter\\Øvrige polymere materialer\\Polyaluminiumklorid#Kemikalier og kemiske produkter\\Jernforbindelser\\Jernklorid#Kemikalier og kemiske produkter\\Kemikalier til forskellige industrier\\Industrikemikalier#Kemikalier og kemiske produkter\\Øvrige polymere materialer#Kemikalier og kemiske produkter\\Aluminium- og ammoniakforbindelser\\Aluminiumsulfat#Kemikalier og kemiske produkter#Kemikalier og kemiske produkter\\Aluminium- og ammoniakforbindelser\\Aluminiumklorid");
		task.executeSearchTask(context);
		List<String> productCategories= task.getProductCategories();
		assert(productCategories.get(0).equals("Jernforbindelser"));
		assert(productCategories.get(3).equals("Afløbsrensning, affaldshåndtering, renholdelse o.lign."));
		assert(productCategories.size()>(2));
	}
}
