package se.nordicnet.cfes.tasks;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import no.comperio.fes.framework.FrontEndServiceHashMapContext;
import no.comperio.fes.search.SearchContext;
import no.comperio.fes.search.common.query.Navigator;
import no.comperio.fes.search.common.query.QueryData;
import no.comperio.fes.search.common.result.NavigationElement;
import no.comperio.fes.search.common.result.NavigationEntry;
import no.comperio.fes.search.common.result.QueryAnalyserMatch;
import no.comperio.fes.search.common.result.ResultData;
import no.comperio.fes.search.common.result.ResultPage;
import no.comperio.fes.search.common.result.ResultSegment;


/**
 * 
 * A utility-file for creating custom ResultData to CFES
 * 
 * @author Christian Rieck - Comperio 2007
 * 
 * **/

public class SearchContextTestUtils {
	// TODO skriv noe for � lese inn cfes-xml og generere et searchContext. 
	
	//print debug values? 
	static boolean debug = true; 
	

	public static final int LINKS = 0; 
	public static final int TREE = 1; 
	private final String links = "links"; 
	private final String tree = "tree"; 
	
	

	
	/**
	 * Generates a set of YYYY-MM-DD dates.
	 * 
	 * @input days - A list of dates to be generated. 0 is today, 1 yesterday etc.
	 * 
	 * 
	 * */
	public String[] generateDays(int[] days) {
		final long secinday = 1000*60*60*24; //ms, really
		
		// need to know what time it is right now
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); 
		long now = new Date().getTime();

		// compute the date based on milliseconds since something.
		// convert from long to date and store in list.
		String[] ret = new String[days.length]; 
		for(int i = 0; i < days.length; i++) {
			long time = now - days[i]*secinday; 
			ret[i] = sdf.format(time); 
		}
		
		return ret; 
	}
	
	/**
	 * 	generate NavigationElements.

		@input days - name of the elements.
		@input counts - the hitcount for each day
		
		@return An array of NavigationElement with the specified names and hit counts.
		
		@throws IllegalArgumentException if days and counts are of uneven lengths
	 * */
	public NavigationElement[] generateNEs(String[] days, int[] counts) throws IllegalArgumentException {
		
		if(days.length != counts.length)
			throw new IllegalArgumentException("GenerateNEs: days and counts are not of equal length"); 
		
		NavigationElement[] els = new NavigationElement[days.length]; 
		for(int i = 0; i < days.length; i++) {
			els[i] = new NavigationElement(); //days[i], counts[i]);
			els[i].setName(days[i]); 
			els[i].setCount(counts[i]); 
			els[i].setQuery(new QueryData()); 
		} 
		
		return els; 
	}
	
	
	/**
	 * 	generate NavigationElements based on generated dates in string-format

		@input days - a list of dates in 2007-12-01 format
		@input counts - the hitcount for each day
		
		@return An array of NavigationElement with the specified names and hit counts.
		
		@throws IllegalArgumentException if days and counts are of uneven lengths
	 * */
	public NavigationElement[] generateDateTimeNavigationEntrys(int[] days, int[] counts) throws IllegalArgumentException {
		String[] dates = generateDays(days); 
		return generateNEs(dates, counts); 
	}
	
	

	
	/**
	 * Instantiates a a SearchContext with a list of ResultSegments (RS).
	 * Each RS will have an empty list of ResultPages.
	 * 
	 * @input names - the list of names for the ResultSegments
	 * @input queries - a list of strings that will be the originalquery in the ResultSegments
	 * 
	 * 
	 * */
	public SearchContext createSearchContext(String[] names, String[] queries) throws IllegalArgumentException {

		SearchContext sct = createSearchContext(); 
		ResultData rd = createResultData(); 
		rd.setSegments(createResultSegmens(names, queries)); 
		sct.setResultData(rd); 
		
		return sct; 

	}
	/**
	 * Instantiates a a SearchContext with a list of ResultSegments (RS).
	 * Each RS will have an empty list of ResultPages.
	 * 
	 * @input name - the name for the ResultSegments
	 * @input query - string that will be the originalquery in the ResultSegments
	 * 
	 * 
	 * */
	public SearchContext createSearchContext(String name, String query) throws IllegalArgumentException {

		SearchContext sct = createSearchContext(); 
		ResultData rd = createResultData(); 
		List<ResultSegment> list = new ArrayList<ResultSegment>(); 
		list.add(createResultSegment(name, query));
		rd.setSegments(list); 
		sct.setResultData(rd); 
		
		
		return sct; 

	}

	/**
	 * Instantiates a a SearchContext with a list of ResultSegments.
	 * The segments will be named Segment1 .. n and have Query1...n as queries.
	 * 
	 * @input segments - the number of segments that should be returned.
	 * 
	 * 
	 * 
	 * */
	public SearchContext createStandardSearchContext(int segments) throws IllegalArgumentException {

		SearchContext sct = createSearchContext(); 
		ResultData rd = createResultData();
		String[] names = new String[segments]; 
		String[] queries = new String[segments]; 
		for(int i = 0; i < segments; i++) {
			names[i] = "Segment"+i; 
			queries[i] = "Query"+i; 
		}
		rd.setSegments(createResultSegmens(names, queries)); 
		sct.setResultData(rd); 
				
		return sct; 

	}

	
	
	/**
	 * 
	 * Create a NavigationEntry with the specified name, display name, style and hitcount.
	 * 
	 * @input name - the name of the NavigationEntry
	 * @input style - either LINKS or TREE
	 * @input style - 
	 * 
	 * */
	
	public NavigationEntry createNavigationEntry(String name, String displayName, int style, int hitCount) throws IllegalArgumentException{
		if(style != LINKS && style != TREE)
			throw new IllegalArgumentException("Parameter style is not a valid style for NavigationEntry."+style); 
		
		NavigationEntry ret = new NavigationEntry(); 
		ret.setName(name); 
		ret.setDisplayName(displayName); 
		ret.setHitCount(hitCount); 
		
		switch(style) {
		case LINKS:
			ret.setStyle(links); 
			break; 
		case TREE: 
			ret.setStyle(tree); 
		}
		
		return ret; 
	}
	
	/**
	 * Instantiates a list of NavigationElements.
	 * 
	 * @input names - the list of names for the NEs
	 * @input counts - how many hits each NE will have.
	 * 
	 * @return An util.List of NavigationElements with the specified names and hit counts.
	 * 	 
	 * @throws IllegalArgumentException if names.length != queries.length
	 * */
	public List<NavigationElement> createNavigationElements(String[] names, int[] gencount) throws IllegalArgumentException {
		
		NavigationElement[] els = generateNEs(names, gencount);

		
		ArrayList<NavigationElement> navels = new ArrayList<NavigationElement>(); 
		for(NavigationElement e : els)
			navels.add(e); 
		
		
		return navels; 
	}

	
	
	/**
	 * returns an empty ResultData object.
	 * 
	 * */
	public ResultData createResultData() {
		ResultData rd = new ResultData(); 
		return rd; 
	}
	
	
	
	
	

	/**
	 * 
	 * Creates and returns an empty SearchContext.
	 * 
	 * */
	private SearchContext createSearchContext() {
		FrontEndServiceHashMapContext tmp = new FrontEndServiceHashMapContext(); 
		SearchContext sct = new SearchContext(tmp);  

		
		
		return sct; 
	}

	
	
	
	/**
	 * Creates a list of ResultSegments (RS) with status completed.
	 * 
	 * @input name - names of the RSs
	 * @input query - will be set as the original query for each RS
	 * 
	 * @throws IllegalArgumentException if names.length != queries.length
	 * */	public List<ResultSegment> createResultSegmens(String[] names, String[] queries) throws IllegalArgumentException {
		if(names.length != queries.length)
			throw new IllegalArgumentException("Names and queries are not of equal length"); 
		
		List<ResultSegment> list = new ArrayList<ResultSegment>(); 
		for(int i = 0; i < names.length; i++)
			list.add(createResultSegment(names[i], queries[i])); 
		
		return list; 
	}
	
	
	/**
	 * Creates a ResultSegment (RS) with status completed.
	 * An QueryData will be created with the specified query and inserted into the RS.
	 * An list with one ResultPage will be added as resultpages.
	 * 
	 * @input name - name of the RS
	 * @input query - will be set as the original query for this RS
	 * 
	 * 
	 * */
	private ResultSegment createResultSegment(String name,  String query) {
		ResultSegment rs = new ResultSegment(); 
		rs.setName(name); 
		rs.setStatus(ResultSegment.Status.COMPLETED); 
		rs.setOriginalQuery(createQueryData(query)); 
		List<ResultPage> pages = new ArrayList<ResultPage>();
		pages.add(new ResultPage()); 
		rs.setResultPages(pages);
		return rs; 
	}
	
	
	
	/**
	 * Creates an empty QueryData object.
	 * 
	 * */
	public QueryData createQueryData(String queryString) {
		QueryData qd = new QueryData(); 
		qd.setQueryString(queryString);
		return qd; 
	}
	
	
	
	/**
	 * Creates an empty resultpage and adds the given list of NavigationEntry-s
	 * 
	 * @return The ResultPage created
	 * **/
	public ResultPage createResultPage(List<NavigationEntry> NEs) {
		ResultPage rp = new ResultPage(); 

		rp.setNavigationEntries(NEs); 
		return rp; 
	}
	
	
	/**
	 * Adds a ResultPage to a given ResultSegment in a SearchContext.
	 * 
	 * @input page - the resultpage to be added
	 * @input sct - The SearchContext where the ResultSegment is found
	 * @input segmentName - The name of the segment that will have the page added.
	 * 
	 * @return True if the page could be added
	 * */
	public boolean addResultPageToSegment(ResultPage page, SearchContext sct, String segmentName) {
		
		ResultSegment seg = sct.getResultData().getSegmentMap().get(segmentName);
		if(null == seg)
			return false; 
		
		seg.addResultPage(page); 
		
		return true; 
	}
	
	
	
	public void printDebug(String s) {
		if(debug)
			System.out.println(s);
	}

	
	/**
	 * Create a Navigator with lower[i], upper[i], key[i] and attach it to list[i]
	 * 
	 * 
	 * @throws IllegalArgumentException if any of the lists have different size
	 * */
	public void addNavigatorsToNavigationElements(
			List<NavigationElement> navels, String[] lower, String[] upper, String key) throws IllegalArgumentException{

		if(!(navels.size() == lower.length && lower.length == upper.length))
			throw new IllegalArgumentException("FrameWork: Lists of unequal length"); 

		int i = 0; 
		for(NavigationElement ne : navels) {
			ne.getQuery().addNavigator(new Navigator(key, "["+lower[i]+";"+upper[i++]+"]", "", "")); 
		}
		
	}

	
	/**
	 * Creates a list of NavigationEntry and returns the list.
	 * 
	 * @param names - A list of names for the entries
	 * @param dispNames - the displaynames of the entries.
	 * 
	 * 
	 * @throws IllegalArgumentException if the lists are of uneven length.
	 * **/
	public List<NavigationEntry> createNavigationEntries(String[] names, String[] dispNames, int[] count) throws IllegalArgumentException{
		// basic error handling
		if(!(names.length == dispNames.length && dispNames.length == count.length))
			throw new IllegalArgumentException("Framework: AddNavigationEntries: names and dispNames and count of unequal length"); 

		// create and return. 
		List<NavigationEntry> entries = new ArrayList<NavigationEntry>(); 
		for(int i = 0; i < names.length; i++)
			entries.add(createNavigationEntry(names[i], dispNames[i], SearchContextTestUtils.LINKS, count[i]));
		
		return entries; 
	}

	
	
	
	
	/**
	 * 
	 * Add the NavigationEntries to a given Segment
	 * 
	 * */
	public void addNavigatorEntriesToSegment(List<NavigationEntry> entries,
			String segmentName, SearchContext sct) {
		
		// get correct segment and insert entries to the resultPage there
		ResultSegment seg = sct.getResultData().getSegment(segmentName); 
		if(null != seg) {
			for(ResultPage rp : seg.getResultPages())
				rp.setNavigationEntries(entries); 
		}

	}

	
	/**
	 * 
	 * Adds the NavigationEntries to all ResultSegments in the search context
	 * 
	 * */
	public void addNavigatorEntriesToAllSegments(List<NavigationEntry> entries, SearchContext sct) {
		
		for(ResultSegment seg : sct.getResultData().getSegments()) 
			for(ResultPage rp : seg.getResultPages())
				rp.setNavigationEntries(entries); 
	}

	
	/**
	 * Adds the NavigationElements to the named NavigationEntry
	 * 
	 * @param navels - The NavigationEntries present in the ResultPage
	 * @param entries - The list of NavigationEntries to add
	 * @param name - The name of the NavigationElement to add the Entries to.
	 * 
	 * @throws CFESUTException if the NEntry cannot be found.
	 * 
	 * */
	public void addNavigatorElementsToNavigationEntry(
			List<NavigationElement> navels, List<NavigationEntry> entries,
			String name)  {

		boolean belch = true; 
		for(NavigationEntry ne : entries) {
			if(ne.getName().equals(name)) {
				ne.setNavigationElements(navels); 
				belch = false; 
			}
		}
		
		if(belch) {
			String error = "AddNavElem2NavEntry - NavigationEntry "+ name +" not found amongst: ";
			for(NavigationEntry e : entries)
				error += e.getName()+" "; 
			throw new RuntimeException(error); 
		}
	}

	
	public void addAttributeToContext(String attributeName) {
//	(ArrayList<QueryAnalyserMatch>) sct.getAttribute(attributeName)

	}
	
	
	/**
	 * Same as createReplyFromQMServer, but with a standard attributeName of "qmresult"
	 * */
	public void createReplyFromQMServerStandard(SearchContext sct, String base, String meta) {
		createReplyFromQMServer(sct, base, meta, "qmresult"); 
	}
	
	/**
	 * Sets up a simple QueryAnalyserMatch with a base and meta. 
	 * 
	 * If the given SearchContext already has a list with QueryAnalyserMatch-es the new QAM is added to this list. 
	 * If not, a list is created and the new QAM is added.
	 * 
	 *	@param SearchContext - the result is added here
	 * 	@param Base - the "base" part of a QueryAnalyserMatch
	 *  @param Meta - the "meta" part of a QueryAnalyserMatch
	 *  @param attributeName - name of the attribute where the list of QAMs are stored in the SearchContext
	 **/
	public void createReplyFromQMServer(SearchContext sct, String base, String meta, String attributeName) {
		QueryAnalyserMatch qam = new QueryAnalyserMatch(); 
		qam.setBase(base); 
		qam.setMeta(meta); 
		

		// add to existing list, or create a new one? 
		@SuppressWarnings("unchecked")
		List<QueryAnalyserMatch> results = (List<QueryAnalyserMatch>)sct.getAttribute(attributeName); 
		if(results == null) {
			results = new ArrayList<QueryAnalyserMatch>(); 
			sct.setAttribute(attributeName, results); 
		}
		
		// add to list.
		results.add(qam); 
	}
	
	
	
	
	/**
	 * Adds a total number of hits to each <code>ResultPage</code> in the given <code>ResultSegment</code>
	 * 
	 * @param sct The SearchContext to be modified
	 * @param hits The totalHits.
	 * */

	public void ResultSetAddTotalHitsAllSegments(SearchContext sct, int hits) {
		if(sct.getResultData() == null) {
			return; 
		}
		List<ResultSegment> segments = sct.getResultData().getSegments();
		
		
		//iterate
		for(ResultSegment segment : segments) {
			if(segment.getResultPages() == null)
				continue; 
			
			List<ResultPage> pages = segment.getResultPages(); 
			
			for(ResultPage page: pages) {
				page.getResultSet().setTotalHits(hits); 
			}
			
		}
	}



	/**
	 * Adds a total number of hits to each <code>ResultPage</code> in the given <code>ResultSegment</code>
	 * 
	 * @param sct The SearchContext to be modified
	 * @param segmentName Name of the ResultSegment to modify
	 * @param hits The totalHits.
	 * */
	public void resultSetAddTotalHits(SearchContext sct, String segmentName, int hits) throws IllegalArgumentException{
		if(sct.getResultData() == null) {
			return; 
		}

		//iterate
		ResultSegment segment = sct.getResultData().getSegment(segmentName); 
		if(segment == null || segment.getResultPages() == null)
			throw new IllegalArgumentException(segmentName + " not found in seachContext"); 

		List<ResultPage> pages = segment.getResultPages(); 

		for(ResultPage page: pages) {
			page.getResultSet().setTotalHits(hits); 
		}

	}
	
}