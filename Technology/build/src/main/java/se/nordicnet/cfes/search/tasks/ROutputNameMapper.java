package se.nordicnet.cfes.search.tasks;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import no.comperio.fes.search.SearchContext;
import no.comperio.fes.search.common.result.Hit;
import no.comperio.fes.search.common.result.NavigationEntry;
import no.comperio.fes.search.common.result.ResultPage;
import no.comperio.fes.search.common.result.ResultSegment;
import no.comperio.fes.search.tasks.AbstractSearchTask;
import no.comperio.workflowengine.conditions.ConditionException;
import no.comperio.workflowengine.conditions.ConditionObjectMap;
import no.comperio.workflowengine.exceptions.ConfigurationException;
import no.comperio.workflowengine.exceptions.ExecutionException;
import no.comperio.workflowengine.tasks.TaskExecutionException;

/**
 * @author Marcus Johansson
 */
public class ROutputNameMapper extends AbstractSearchTask {

	private String segmentName;
	private ConditionObjectMap<Map<String, String>> nameMap;
	private Logger log = Logger.getLogger(ROutputNameMapper.class);

	@Override
	public void executeSearchTask(SearchContext context) throws TaskExecutionException, ExecutionException {
		Map<String, String> mapping = null;

		try {
			mapping = nameMap.evaluate(context);
		} catch (ConditionException e) {
			throw new TaskExecutionException(this, "Name mapping evaluation error", e);
		} catch (ConfigurationException e) {
			throw new TaskExecutionException(this, "Name mapping evaluation error", e);
		}

		// Mapping is null if no conditions match
		if (mapping != null) {
			ResultSegment segment = context.getResultData().getSegment(this.segmentName);
			for (ResultPage page : segment.getResultPages()) {

				// Map names in navigators
				List<NavigationEntry> newNavEntries = new ArrayList<NavigationEntry>();
				for (NavigationEntry navEntry : page.getNavigationEntries()) {
					navEntry.setName(getTranslation(navEntry.getName(), mapping));
					navEntry.setFieldName(getTranslation(navEntry.getFieldName(), mapping));
					newNavEntries.add(navEntry);
				}
				page.setNavigationEntries(newNavEntries);

				// Map names in hit fields
				for (Hit hit : page.getResultSet().getHits()) {
					Map<String, String> newFields = new HashMap<String, String>();
					for (String fieldName : hit.getFields().keySet()) {
						log.info(fieldName);
						newFields.put(getTranslation(fieldName, mapping), hit.getFieldValue(fieldName));
					}
					hit.setFields(newFields);
				}
			}
		}
	}

	private String getTranslation(String name, Map<String, String> mapping) {
		if (mapping.containsKey(name)) {
			return mapping.get(name);
		} else {
			return name;
		}
	}

	public void setSegmentName(String segmentName) {
		this.segmentName = segmentName;
	}

	public void setNameMap(ConditionObjectMap<Map<String, String>> nameMap) {
		this.nameMap = nameMap;
	}

}
