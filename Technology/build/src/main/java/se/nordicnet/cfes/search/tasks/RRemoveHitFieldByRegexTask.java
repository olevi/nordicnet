package se.nordicnet.cfes.search.tasks;

import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import java.util.HashMap;
import java.util.Map;

import no.comperio.workflowengine.tasks.TaskConfigurationException;
import no.comperio.workflowengine.tasks.TaskExecutionException;
import no.comperio.fes.search.SearchContext;
import no.comperio.fes.search.common.result.ResultSegment;
import no.comperio.fes.search.common.result.ResultPage;
import no.comperio.fes.search.common.result.Hit;
import no.comperio.fes.search.tasks.AbstractSearchTask;

/**
 * @author Marcus Johansson
 */
public class RRemoveHitFieldByRegexTask extends AbstractSearchTask {

	private String segmentName;
	private String regex;
	private Pattern regexCompiled;

	@Override
	public void afterPropertiesSet() throws TaskConfigurationException {
		if (this.segmentName == null) {
			throw new TaskConfigurationException(this, "Field 'segmentName' cannot be empty");
		}
		if (this.regex == null) {
			throw new TaskConfigurationException(this, "Field 'regex' cannot be empty");
		}
		try {
			this.regexCompiled = Pattern.compile(this.regex);
		} catch (PatternSyntaxException e) {
			throw new TaskConfigurationException(this, "Pattern " + this.regex + "in field \'regex\' does not compile");
		}
	}

	@Override
	public void executeSearchTask(SearchContext context) throws TaskExecutionException {
		ResultSegment segment = context.getResultData().getSegment(this.segmentName);
		if (segment != null) {
			for (ResultPage resultPage : segment.getResultPages()) {
				for (Hit hit : resultPage.getResultSet().getHits()) {

					// Create a new set of fields
					Map<String, String> newFieldMap = new HashMap<String, String>();

					// Add fields NOT matching the regex to the new set
					for (String fieldName : hit.getFields().keySet()) {
						if (!this.regexCompiled.matcher(fieldName).matches()) {
							newFieldMap.put(fieldName, hit.getFieldValue(fieldName));
						}
					}

					// Overwrite the old field set
					hit.setFields(newFieldMap);
				}
			}
		}
	}

	public void setSegmentName(String segmentName) {
		this.segmentName = segmentName;
	}

	public void setRegex(String regex) {
		this.regex = regex;
	}

}
