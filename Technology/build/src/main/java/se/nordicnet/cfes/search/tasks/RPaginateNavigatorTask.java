package se.nordicnet.cfes.search.tasks;

import java.util.ArrayList;
import java.util.List;

import no.comperio.fes.search.SearchContext;
import no.comperio.fes.search.common.result.NavigationElement;
import no.comperio.fes.search.common.result.NavigationEntry;
import no.comperio.fes.search.common.result.ResultPage;
import no.comperio.fes.search.common.result.ResultSegment;
import no.comperio.fes.search.tasks.AbstractSearchTask;
import no.comperio.workflowengine.exceptions.ExecutionException;
import no.comperio.workflowengine.tasks.TaskConfigurationException;
import no.comperio.workflowengine.tasks.TaskExecutionException;
import no.comperio.workflowengine.util.Expression;

/**
 * @author Pontus Hym�r
 */
public class RPaginateNavigatorTask extends AbstractSearchTask {

	private String segmentName;
	private String navigatorName;
	private String navigatorNameExpression;
	private String offsetExpression;
	private String bucketSizeExpression;
	private int offset;
	private int bucketSize;

	@Override
	public void afterPropertiesSet() throws TaskConfigurationException {
		if (this.segmentName == null) {
			throw new TaskConfigurationException(this, "Parameter 'segmentName' must be set");
		}
		if (this.bucketSizeExpression == null) {
			this.bucketSizeExpression = "0";
		}
	}

	@Override
	public void executeSearchTask(SearchContext context) throws TaskExecutionException, ExecutionException {
		// What attributes can be removed?
		String[] arAttributesToRemove = {"searchprofile","navsize","navoffset"};
		
		Expression<String> offsetExpression = new Expression<String>(this.offsetExpression);
		this.offset = Integer.parseInt(offsetExpression.evaluate(context));
		Expression<String> bucketSizeExpression = new Expression<String>(this.bucketSizeExpression);
		this.bucketSize = Integer.parseInt(bucketSizeExpression.evaluate(context));
		Expression<String> navigatorNameExpression = new Expression<String>(this.navigatorNameExpression);
		this.navigatorName = navigatorNameExpression.evaluate(context);
		
		ResultSegment segment = context.getResultData().getSegment(this.segmentName);
		if (segment != null) {
			for (ResultPage resultPage : segment.getResultPages()) {
				NavigationEntry navEntry = resultPage.getNavigationEntryByName(this.navigatorName);
				if (navEntry != null) {
					List<NavigationElement> navElemList = navEntry.getNavigationElements();
					// Clean away unnecessary parameters
					for (NavigationElement navigationElement : navElemList) {
						for (String attrKey : arAttributesToRemove) {
							// Remove specified attributes not needed for links
							navigationElement.getQuery().removeAttributeByKey(attrKey);
						}
					}
					navEntry.setUnit(Integer.toString(navElemList.size()));
					if(bucketSize>0)
						navEntry.setNavigationElements(new ArrayList<NavigationElement>(navElemList.subList(this.offset, Math.min(this.offset+this.bucketSize,navElemList.size()))));
				}
			}
		}
	}

	public String getSegmentName() {
		return segmentName;
	}

	public void setSegmentName(String segmentName) {
		this.segmentName = segmentName;
	}

	public String getNavigatorName() {
		return navigatorName;
	}

	public void setNavigatorName(String navigatorName) {
		this.navigatorName = navigatorName;
	}

	public String getOffsetExpression() {
		return offsetExpression;
	}

	public void setOffsetExpression(String offsetExpression) {
		this.offsetExpression = offsetExpression;
	}

	public String getBucketSizeExpression() {
		return bucketSizeExpression;
	}

	public void setBucketSizeExpression(String bucketSizeExpression) {
		this.bucketSizeExpression = bucketSizeExpression;
	}

	public String getNavigatorNameExpression() {
		return navigatorNameExpression;
	}

	public void setNavigatorNameExpression(String navigatorNameExpression) {
		this.navigatorNameExpression = navigatorNameExpression;
	}
}
