package se.nordicnet.cfes.search.tasks;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import no.comperio.workflowengine.tasks.TaskConfigurationException;
import no.comperio.workflowengine.tasks.TaskExecutionException;
import no.comperio.fes.search.SearchContext;
import no.comperio.fes.search.common.result.Hit;
import no.comperio.fes.search.common.result.NavigationElement;
import no.comperio.fes.search.common.result.NavigationEntry;
import no.comperio.fes.search.common.result.ResultSegment;
import no.comperio.fes.search.tasks.AbstractSearchTask;

/**
 * @author Marcus Johansson
 */
public class RFilterAndSortSubElementsTask extends AbstractSearchTask {

	private String segmentName;
	private String fieldName;
	private String navigatorName;

	private String separator;
	private String subString;
	private Boolean sortBySubString;

	@Override
	public void afterPropertiesSet() throws TaskConfigurationException {
		if (this.segmentName == null) {
			throw new TaskConfigurationException(this, "Field 'segmentName' cannot be empty");
		}
		if (this.fieldName == null && this.navigatorName == null) {
			throw new TaskConfigurationException(this, "Either of  \'fieldName\' and \'navigatorName\' must be set");
		}
		if (this.fieldName != null && this.navigatorName != null) {
			throw new TaskConfigurationException(this, "Both \'fieldName\' and \'navigatorName\' can't be set at the same time");
		}
	}

	@Override
	public void executeSearchTask(SearchContext context) throws TaskExecutionException {
		ResultSegment segment = context.getResultData().getSegment(this.segmentName);
		if (segment != null) {
			// TODO: Expose in Front configuration using JEXL
			this.subString = context.getQueryData().getQueryString();

			// TODO: Expose in Front configuration
			this.sortBySubString = true;

			// TODO: Expose in Front configuration
			this.separator = "#";

			// NAVIGATOR:
			if (this.navigatorName != null) {

				// Fetch elements
				List<String> aList = new ArrayList<String>();
				NavigationEntry navigator = segment.getResultPages().get(0).getNavigationEntryByName(this.navigatorName);
				if (navigator == null) {
					return; // No such navigator, search to narrow?
				}
				for (NavigationElement element : navigator.getNavigationElements()) {
					aList.add(element.getName());
				}

				// Filter elements
				aList = this.removeNonMatches(aList, this.subString);

				// Sort elements, if wanted
				if (this.sortBySubString) {
					Collections.sort(aList, new SubStringComparator(this.subString));
				}

				// Recreate the NavigationEntry accordingly
				List<NavigationElement> newList = new ArrayList<NavigationElement>();
				for (String navigatorName : aList) {
					newList.add(navigator.getNavigationElement(navigatorName));
				}
				navigator.setNavigationElements(newList);

				// Remove NavigationEntry node if there are no elements left
				if (newList.size() == 0) {
					List<NavigationEntry> navigationEntries = segment.getResultPages().get(0).getNavigationEntries();
					navigationEntries.remove(navigator);
					segment.getResultPages().get(0).setNavigationEntries(navigationEntries);
				} else {
					// If there are elements, update count (bucket count)
					navigator.setHitCount(newList.size());
				}

			} else {
				// MULTI-VALUE FIELD:
				if (!segment.getResultPages().isEmpty()) {
					for (Hit hit : segment.getResultPages().get(0).getResultSet().getHits()) {
						String field = hit.getFieldValue(this.fieldName);
						if (field != null) {

							// Fetch elements
							List<String> aList = new ArrayList<String>();
							for (String value : field.split(this.separator)) {

								// Ugly fix: Removes product id in field products
								if (this.fieldName.equals("products")) {
									value = value.replaceAll("[[0-9]|\\$]", "");
								}

								aList.add(value);
							}

							// Filter elements
							// This should not be triggered on hit values?
							// aList = this.removeNonMatches(aList, this.subString);

							// Sort elements, if wanted
							if (this.sortBySubString) {
								Collections.sort(aList, new SubStringComparator(this.subString));
							}

							// Recreate the multi-value field accordingly
							String newField = "";
							for (String value : aList) {
								newField += this.separator + value;
							}
							if (newField.length() > 0) {
								// Remove first separator
								newField = newField.substring(1);
							}
							hit.setFieldValue(this.fieldName, newField);
						}
					}
				}
			}
		}
	}

	/*
	 * Remove strings not matching a certain substring from a list
	 */
	public List<String> removeNonMatches(List<String> aList, String subString) {
		// Populate list with elements to be deleted (without the substring)
		List<String> tempList = new ArrayList<String>();
		for (String elem : aList) {
			if (this.removeHTML(elem.toLowerCase()).contains(subString.toLowerCase())) {
				tempList.add(elem);
			}
		}
		return tempList;
	}

	/*
	 * Implements Nordicnet's sorting algorithm, see in-line comments
	 */
	public class SubStringComparator implements Comparator<String> {

		private String subString;

		public SubStringComparator(String subString) {
			this.subString = subString;
		}

		public int compare(String str1, String str2) {
			str1 = RFilterAndSortSubElementsTask.this.removeHTML(str1).toLowerCase();
			str2 = RFilterAndSortSubElementsTask.this.removeHTML(str2).toLowerCase();

			// If only Java would have closures... :-(

			// #1. Identical match
			if (str1.equals(this.subString) && str2.equals(this.subString)) {
				return 0;
			} else {
				if (str1.equals(this.subString)) {
					return -1;
				}
				if (str2.equals(this.subString)) {
					return 1;
				}
			}

			// #2. Prefix match with trailing space
			if (str1.startsWith(this.subString + " ") && str2.startsWith(this.subString + " ")) {
				return 0;
			} else {
				if (str1.startsWith(this.subString + " ")) {
					return -1;
				}
				if (str2.startsWith(this.subString + " ")) {
					return 1;
				}
			}

			// #3. Substring match with spaces around or EOL behind
			if ((str1.endsWith(" " + this.subString) || str1.contains(" " + this.subString + " ")) && (str2.endsWith(" " + this.subString) || str2.contains(" " + this.subString + " "))) {
				return 0;
			} else {
				if (str1.endsWith(" " + this.subString) || str1.contains(" " + this.subString + " ")) {
					return -1;
				}
				if (str2.endsWith(" " + this.subString) || str2.contains(" " + this.subString + " ")) {
					return 1;
				}
			}

			// #4. Prefix match
			if (str1.startsWith(this.subString) && str2.startsWith(this.subString)) {
				return 0;
			} else {
				if (str1.startsWith(this.subString)) {
					return -1;
				}
				if (str2.startsWith(this.subString)) {
					return 1;
				}
			}

			// #5. Inner prefix match
			if (str1.contains(" " + this.subString) && str2.contains(" " + this.subString)) {
				return 0;
			} else {
				if (str1.contains(" " + this.subString)) {
					return -1;
				}
				if (str2.contains(" " + this.subString)) {
					return 1;
				}
			}

			// #6. Substring match (already ensured by filtering)

			// #7. Alphabetical
			return str1.compareTo(str2);
		}
	}

	/*
	 * Removes HTML, necessary for filtering/sorting if hit is highlighted
	 */
	public String removeHTML(String str) {
		return str.replaceAll("\\<.*?\\>", "");
	}

	public void setSegmentName(String segmentName) {
		this.segmentName = segmentName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public void setNavigatorName(String navigatorName) {
		this.navigatorName = navigatorName;
	}

}
