package se.nordicnet.cfes.search.tasks;

import java.util.HashMap;
import java.util.Map;

import no.comperio.fes.search.SearchContext;
import no.comperio.fes.search.common.result.ResultSegment;
import no.comperio.fes.search.common.result.Hit;
import no.comperio.fes.search.tasks.AbstractSearchTask;
import no.comperio.workflowengine.tasks.TaskConfigurationException;
import no.comperio.workflowengine.tasks.TaskExecutionException;

/**
 * @author Marcus Johansson
 */
public class RExtractFieldValues extends AbstractSearchTask {

	private String segmentName;
	private String fieldName;
	private String target;
	private String sourceSeparator = "#";
	private String targetSeparator = "#";

	@Override
	public void afterPropertiesSet() throws TaskConfigurationException {
		if (this.segmentName == null) {
			throw new TaskConfigurationException(this, "Parameter 'segmentName' must be set");
		}
		if (this.fieldName == null) {
			throw new TaskConfigurationException(this, "Parameter 'fieldName' must be set");
		}
		if (this.target == null) {
			throw new TaskConfigurationException(this, "Parameter 'target' must be set");
		}
	}

	@Override
	public void executeSearchTask(SearchContext context) throws TaskExecutionException {
		Map<String, Object> values = new HashMap<String, Object>();

		// Get values from each hit, take multiple fields into account and
		// remove duplicates
		ResultSegment segment = context.getResultData().getSegment(this.segmentName);
		if (segment != null) {
			for (Hit hit : segment.getResultPages().get(0).getResultSet().getHits()) {
				String fieldValue = hit.getFieldValue(this.fieldName);
				if (fieldValue != null) {
					for (String value : fieldValue.split(this.sourceSeparator)) {
						values.put(value, null);
					}
				}
			}

			// Build output, make sure to remove first separator
			String output = "";
			for (String value : values.keySet()) {
				output += this.targetSeparator + value;
			}
			output = (output.length() > 0) ? output.substring(this.targetSeparator.length()) : "";

			context.setAttribute(this.target, output);
		}
	}

	public void setSegmentName(String segmentName) {
		this.segmentName = segmentName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public void setSourceSeparator(String sourceSeparator) {
		this.sourceSeparator = sourceSeparator;
	}

	public void setTargetSeparator(String targetSeparator) {
		this.targetSeparator = targetSeparator;
	}

}
