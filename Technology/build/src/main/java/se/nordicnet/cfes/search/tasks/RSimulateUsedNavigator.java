package se.nordicnet.cfes.search.tasks;

//import java.util.List;

import no.comperio.fes.search.SearchContext;
//import no.comperio.fes.search.common.query.Modifier;
//import no.comperio.fes.search.common.query.Navigator;
import no.comperio.fes.search.common.result.NavigationElement;
import no.comperio.fes.search.common.result.NavigationEntry;
import no.comperio.fes.search.common.result.ResultPage;
import no.comperio.fes.search.common.result.ResultSegment;
//import no.comperio.fes.search.common.result.UsedModifier;
//import no.comperio.fes.search.common.result.UsedNavigator;
import no.comperio.fes.search.tasks.AbstractSearchTask;
import no.comperio.workflowengine.exceptions.ExecutionException;
import no.comperio.workflowengine.tasks.TaskExecutionException;
import no.comperio.workflowengine.util.Expression;

public class RSimulateUsedNavigator extends AbstractSearchTask {

	private String segmentName;
	private String navigationEntryName;
	private String termExpression;

	@Override
	public void executeSearchTask(SearchContext context) throws TaskExecutionException, ExecutionException {
		
		ResultSegment segment = context.getResultData().getSegment(this.segmentName);
		if (segment != null) {
			if (!segment.getResultPages().isEmpty()) {
				ResultPage resultPage = segment.getResultPages().get(0);

				// Fetch specified NavigationEntry
				NavigationEntry navEntry = resultPage.getNavigationEntryByName(this.navigationEntryName);
				if (navEntry != null) {
					
					// Evaluate termExpression
					String term = new Expression<String>(this.termExpression).evaluate(context);

					// Loop over NavigationElements, look for one with a name matching our 'term'
					for (NavigationElement navElem : navEntry.getNavigationElements()) {
						String tmpValue = navElem.getName();
						if (tmpValue.toLowerCase().equals(term.toLowerCase())) {

							// If this NavigationElement is already in use, we don't need to do this at all
							if(navElem.isInUse()) {
								break;
							}
							navElem.setInUse(true);

							// Add the NavigationElement to the list of used navigators:
							// 1. Get the existing list of used navigators
/*							List<UsedNavigator> usedNavs = resultPage.getUsedNavigators();
							
							// 2. Extract data from the NavigationElement matching our 'term'
							Modifier mod = navElem.getModifierInUse();
							Navigator navInUse = navElem.getNavigatorInUse();
							
							// 3. Create a new UsedNavigator if necessary
							boolean createdNewUsedNav = false;
							UsedNavigator usedNav = null;
							for(UsedNavigator existingUsedNav : usedNavs) {
								if(existingUsedNav.getNavigator().getDisplayName().equalsIgnoreCase(navInUse.getDisplayName())) {
									usedNav = existingUsedNav;
									//usedNavs.remove(existingUsedNav);
									break;
								}
							}
							if(usedNav == null) {
								createdNewUsedNav = true;
								usedNav = new UsedNavigator(navInUse, context.getQueryData());
							}
							
							// 4. Create a new UsedModifier (see the inUse-if-clause above)
							UsedModifier usedMod = new UsedModifier(mod, context.getQueryData());
							
							// 5. Add it to the list of used modifiers
							usedNav.addUsedModifier(usedMod.getModifier(), context.getQueryData());
							
							// 6. If we created this UsedNavigator, we'll have to add it to the list
							if(createdNewUsedNav) {
								//usedNavs.add(usedNav);
							}
							// 7. Don't bother looping over more elements. We're done here.*/
							break;
						}
					}
				}
			}
		}
	}

	public void setSegmentName(String segmentName) {
		this.segmentName = segmentName;
	}

	public void setNavigationEntryName(String navigationEntryName) {
		this.navigationEntryName = navigationEntryName;
	}

	public void setTermExpression(String termExpression) {
		this.termExpression = termExpression;
	}
}
