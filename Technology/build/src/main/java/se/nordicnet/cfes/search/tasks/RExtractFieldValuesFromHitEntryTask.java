package se.nordicnet.cfes.search.tasks;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import no.comperio.fes.search.SearchContext;
import no.comperio.fes.search.common.result.Hit;
import no.comperio.fes.search.common.result.ResultPage;
import no.comperio.fes.search.common.result.ResultSegment;
import no.comperio.fes.search.common.result.ResultSet;
import no.comperio.fes.search.tasks.AbstractSearchTask;
import no.comperio.workflowengine.exceptions.ConfigurationException;
import no.comperio.workflowengine.tasks.TaskConfigurationException;
import no.comperio.workflowengine.tasks.TaskExecutionException;

/**
 * @author Thea Christine Steen
 * 
 */
public class RExtractFieldValuesFromHitEntryTask extends AbstractSearchTask {

	private String segmentName;
	private String fieldName;
	private String targetAttribute;
	private String targetSeparator;
	private String splitRegExp;
	private int numberOfHits = 10;

	@Override
	public void afterPropertiesSet() throws TaskConfigurationException, ConfigurationException {
		// TODO Auto-generated method stub
		super.configurationChanged();

		if (fieldName == null || fieldName == "") {
			throw new TaskConfigurationException(this, "Parameter 'fieldName' must be set.");
		}
		if (segmentName == null || segmentName == "") {
			throw new TaskConfigurationException(this, "Parameter 'segmentName' must be set");
		}
		if (targetSeparator == null) {
			targetSeparator = ", ";
		}
		if (splitRegExp == null) {
			splitRegExp = "#";
		}
	}

	@Override
	public void executeSearchTask(SearchContext context) throws TaskExecutionException {

		List<ResultPage> pages;
		ResultSet resultSet;
		List<Hit> hits;
		String fieldValue;
		List<String> fieldValueList = new ArrayList<String>();

		ResultSegment segment = context.getResultData().getSegment(this.segmentName);
		pages = segment.getResultPages();

		for (ResultPage page : pages) {

			resultSet = page.getResultSet();
			hits = resultSet.getHits();

			for (Hit hit : hits) {
				fieldValue = hit.getFieldValue(fieldName);
				if (fieldValue != null) {
					for (String value : fieldValue.split(this.splitRegExp)) {
						fieldValueList.add(value);
					}
				}
			}
		}

		if (fieldValueList.size() > 0) {
			context.setAttribute(this.targetAttribute, convertToString(sortList(fieldValueList)));
		}

	}

	private List<String> sortList(List<String> list) {

		Map<String, Integer> map = new HashMap<String, Integer>();
		for (String item : list) {
			if (!map.containsKey(item)) {
				map.put(item, 1);
			} else {
				map.put(item, map.get(item) + 1);
			}
		}

		Collections.sort(list, new OccurenceComparator(map));
		List<String> dedupedAndSortedList = new ArrayList<String>();
		for (String item : list) {
			if (!dedupedAndSortedList.contains(item)) {
				dedupedAndSortedList.add(item);
			}
		}

		if (dedupedAndSortedList.size() > this.numberOfHits)
			dedupedAndSortedList = dedupedAndSortedList.subList(0, this.numberOfHits);
		
		return dedupedAndSortedList;
	}

	private class OccurenceComparator implements Comparator<String> {
		private Map<String, Integer> map;

		public OccurenceComparator(Map<String, Integer> map) {
			this.map = map;
		}

		public int compare(String key1, String key2) {
			if (this.map.get(key1) > this.map.get(key2)) {
				return -1;
			}
			if (this.map.get(key1) < this.map.get(key2)) {
				return 1;
			}
			return 0;
		}
	}

	private String convertToString(List<String> fieldValueList) {

		String output = "";
		for (String value : fieldValueList) {
			output += this.targetSeparator + value;
		}
		output = (fieldValueList.size() > 0) ? output.substring(this.targetSeparator.length()) : "";

		return output;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getSegmentName() {
		return segmentName;
	}

	public void setSegmentName(String segmentName) {
		this.segmentName = segmentName;
	}

	public String getTargetAttribute() {
		return targetAttribute;
	}

	public void setTargetAttribute(String targetAttribute) {
		this.targetAttribute = targetAttribute;
	}

	public String getSplitRegExp() {
		return splitRegExp;
	}

	public void setSplitRegExp(String splitRegExp) {
		this.splitRegExp = splitRegExp;
	}

	public String getTargetSeparator() {
		return targetSeparator;
	}

	public void setTargetSeparator(String targetSeparator) {
		this.targetSeparator = targetSeparator;
	}

	public int getNumberOfHits() {
		return this.numberOfHits;
	}

	public void setNumberOfHits(int numberOfHits) {
		this.numberOfHits = numberOfHits;
	}

}
