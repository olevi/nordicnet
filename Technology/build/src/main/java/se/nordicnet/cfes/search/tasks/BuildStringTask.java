package se.nordicnet.cfes.search.tasks;

import java.util.Map;

import no.comperio.fes.search.SearchContext;
import no.comperio.fes.search.tasks.AbstractSearchTask;
import no.comperio.workflowengine.tasks.TaskConfigurationException;
import no.comperio.workflowengine.tasks.TaskExecutionException;
import no.comperio.workflowengine.util.Expression;

/**
 * @author Marcus Johansson
 */
public class BuildStringTask extends AbstractSearchTask {

	private String target;
	private String formatString;
	private String sentinel = "%";
	private Map<String, String> variableExpressions;

	@Override
	public void afterPropertiesSet() throws TaskConfigurationException {
		if (this.target == null) {
			throw new TaskConfigurationException(this, "Parameter 'target' must be set");
		}
		if (this.formatString == null) {
			throw new TaskConfigurationException(this, "Parameter 'formatString' must be set");
		}
	}

	@Override
	public void executeSearchTask(SearchContext context) throws TaskExecutionException {
		String formattedString = this.formatString;
		
		if(this.variableExpressions != null) {
			for (String key : this.variableExpressions.keySet()) {
				// Try to evaluate the value of this key
				Expression<String> variableExpression = new Expression<String>(this.variableExpressions.get(key));
				String value = variableExpression.evaluate(context);
				if (value == null) {
					value = "";
				}
	
				// Do the replacement
				formattedString = formattedString.replace(this.sentinel + key + this.sentinel, value);
	
			}
		}
		context.setAttribute(this.target, formattedString);	
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public void setFormatString(String formatString) {
		this.formatString = formatString;
	}

	public void setSentinel(String sentinel) {
		this.sentinel = sentinel;
	}

	public void setVariableExpressions(Map<String, String> variableExpressions) {
		this.variableExpressions = variableExpressions;
	}

}
