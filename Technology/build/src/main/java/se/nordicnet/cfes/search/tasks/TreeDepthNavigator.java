package se.nordicnet.cfes.search.tasks;

import java.util.Iterator;

import no.comperio.fes.search.SearchContext;
import no.comperio.fes.search.common.result.NavigationEntry;
import no.comperio.fes.search.common.result.NavigationNode;
import no.comperio.fes.search.common.result.ResultData;
import no.comperio.fes.search.common.result.ResultPage;
import no.comperio.fes.search.common.result.ResultSegment;
import no.comperio.fes.search.tasks.AbstractSearchTask;
import no.comperio.workflowengine.tasks.TaskExecutionException;
import no.comperio.workflowengine.util.Expression;

import org.springframework.util.StringUtils;

/**
 * This class allows controller the depth of the navigator tree (producthierarchy-sve)
 * by providing a attribute depth=<1-3> to the URL.
 * @author Daniel Ling (daniel.ling@comperio.no)
 */
public class TreeDepthNavigator extends AbstractSearchTask {

	private static final String DEFAULT_NAVIGATOR_NAME = "producthierarchy-sve";
	private static final String DEPTH_ATTRIBUTE_NAME = "depth";
	private static final String DEFAULT_SEGMENT_NAME = "companies";
	private static final String ROOT_NAVIGATOR_NAME = "root";
	
	private String navigatorName;
	
	public void executeSearchTask( final SearchContext ctx ) throws TaskExecutionException {
		
		String depth = (String) ctx.getQueryData().getAttribute(DEPTH_ATTRIBUTE_NAME);

		if (StringUtils.hasText(depth) && depth.length() == 1) {

			ResultData result = ctx.getResultData();
			ResultSegment resultSegment = result.getSegment(DEFAULT_SEGMENT_NAME);

			if (resultSegment != null) {

				for( ResultPage page : resultSegment.getResultPages() ) {

					if (page.getNavigationEntries() != null) {
						Iterator<NavigationEntry> entryIterator = page.getNavigationEntries().iterator();

						while (entryIterator.hasNext()) {
							NavigationEntry navigationEntry = entryIterator.next();
							String navigatorName = navigationEntry.getDisplayName();
							if (navigatorName.equalsIgnoreCase(getNavigatorName(ctx))) {
								Iterator<NavigationNode> nodeIterator = navigationEntry.getNavigationNodes().iterator();
								while (nodeIterator.hasNext()) {
									NavigationNode rootNode = nodeIterator.next();
									String elementNameTemp = rootNode.getLabel();
									if (StringUtils.hasText(elementNameTemp) && elementNameTemp.equalsIgnoreCase(ROOT_NAVIGATOR_NAME)) {

										int maxDepth = 5;
										try { maxDepth = Integer.parseInt(depth); }
										catch (Exception e) { maxDepth = 5; }

										if (rootNode.getChildNodes().size() > 0) {
											handleNavigationNode(rootNode, maxDepth);
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	public void handleNavigationNode(NavigationNode node, int removeOver) {
		Iterator<NavigationNode> underRootIterate = node.getChildNodes().iterator();
		while (underRootIterate.hasNext()) {
			NavigationNode underRootNode = underRootIterate.next();
			if (underRootNode.getLevel() > removeOver) { 
				underRootIterate.remove();
			}
			else {
				if (node.getChildNodes() != null && node.getChildNodes().size() > 0) { handleNavigationNode(underRootNode, removeOver); }
			}
		}
	}
	
	public void setNavigatorName(String navigatorName) {
		this.navigatorName = navigatorName;
	}
	
	public String getNavigatorName(SearchContext ctx) {
		if (!StringUtils.hasText(navigatorName)) { this.navigatorName = DEFAULT_NAVIGATOR_NAME; }
		Expression<String> navigatorNameExpression = new Expression<String>(this.navigatorName);
		String tempNavigatorName = navigatorNameExpression.evaluate(ctx);
		if (!StringUtils.hasText(tempNavigatorName)) { return DEFAULT_NAVIGATOR_NAME; }
		else { return tempNavigatorName; }
	}

}
