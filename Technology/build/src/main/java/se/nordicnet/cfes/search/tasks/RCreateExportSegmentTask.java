package se.nordicnet.cfes.search.tasks;

import java.util.List;

import no.comperio.fes.search.SearchContext;
import no.comperio.fes.search.common.result.Hit;
import no.comperio.fes.search.common.result.NavigationElement;
import no.comperio.fes.search.common.result.ResultPage;
import no.comperio.fes.search.common.result.ResultData;
import no.comperio.fes.search.common.result.ResultSegment;
import no.comperio.fes.search.common.result.ResultSet;
import no.comperio.fes.search.common.result.ResultSegment.Status;
import no.comperio.fes.search.tasks.AbstractSearchTask;
import no.comperio.workflowengine.tasks.TaskConfigurationException;
import no.comperio.workflowengine.tasks.TaskExecutionException;


/**
 * @author Pontus Hym�r
 */
public class RCreateExportSegmentTask extends AbstractSearchTask {

	private String segmentName;
	private String navigatorName;

	@Override
	public void afterPropertiesSet() throws TaskConfigurationException {
		if (this.segmentName == null) {
			throw new TaskConfigurationException(this,
					"Field 'segmentName' cannot be empty");
		}
		if (this.navigatorName == null) {
			throw new TaskConfigurationException(this,
					"Field 'navigatorName' cannot be empty");
		}
	}

	@Override
	public void executeSearchTask(SearchContext context)
			throws TaskExecutionException {

		ResultData rd = context.getResultData();

		List<ResultSegment> segmentList = context.getResultData().getSegments();
		List<NavigationElement> elementList = segmentList.get(0).getResultPages().get(0).getNavigationEntryByName(this.navigatorName).getNavigationElements();

		if (rd.getSegments().get(0).getResultPages().get(0).getNavigationEntryByName(this.navigatorName) != null) {

			ResultSegment resultSegment = new ResultSegment();
			resultSegment.setName(this.segmentName);
			String strCompanyids = "";

			for (NavigationElement navEl : segmentList.get(0).getResultPages().get(0).getNavigationEntryByName(this.navigatorName).getNavigationElements()) {
				strCompanyids = strCompanyids + "," + navEl.getValue();
			}
			strCompanyids = strCompanyids.substring(2);

			ResultSet frontResultSet = new ResultSet();
			Hit infoHit = new Hit();
			List<Hit> hitList = frontResultSet.getHits();
			infoHit.addField("companyids", strCompanyids);
			infoHit.addField("numberOfHits", String.valueOf(elementList.size()));
			ResultPage resultPage = new ResultPage();
			resultPage.setName("exportdata");
			hitList.add(infoHit);
			frontResultSet.setHits(hitList);
			resultPage.setResultSet(frontResultSet);
			resultSegment.addResultPage(resultPage);
			resultSegment.setStatus(Status.COMPLETED);
			segmentList.add(resultSegment);
			context.getResultData().setSegments(segmentList);
		}

	}

	public void setSegmentName(String segmentName) {
		this.segmentName = segmentName;
	}

	public void setNavigatorName(String navigatorName) {
		this.navigatorName = navigatorName;
	}
}
