package se.nordicnet.cfes.search.tasks;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import no.comperio.fes.search.SearchContext;
import no.comperio.fes.search.common.query.QueryData;
import no.comperio.fes.search.common.result.NavigationElement;
import no.comperio.fes.search.common.result.NavigationEntry;
import no.comperio.fes.search.common.result.ResultPage;
import no.comperio.fes.search.common.result.ResultSegment;
import no.comperio.fes.search.tasks.AbstractSearchTask;
import no.comperio.workflowengine.exceptions.ExecutionException;
import no.comperio.workflowengine.tasks.TaskConfigurationException;
import no.comperio.workflowengine.tasks.TaskExecutionException;
import no.comperio.workflowengine.util.Expression;

/**
 * @author Pontus Hym�r
 * 
 * @description Injects navigation element at end of list with given parameters
 */
public class RInjectNavigationElementTask extends AbstractSearchTask {

	private String segmentName;
	private String navigatorNameExpression;
	private String navigatorName;
	private String elementName;
	private Map<String, String> parameters;
	private Expression<String> parameterValueExpression;
	private String parameterValue;


	@Override
	public void afterPropertiesSet() throws TaskConfigurationException {
		if (this.segmentName == null) {
			throw new TaskConfigurationException(this,
					"Parameter 'segmentName' must be set");
		}
		if (this.navigatorNameExpression == null) {
			throw new TaskConfigurationException(this,
					"Parameter 'navigatorNameExpression' must be set");
		}
		if (this.elementName == null) {
			throw new TaskConfigurationException(this,
					"Parameter 'elementName' must be set");
		}
	}

	@Override
	public void executeSearchTask(SearchContext context)
			throws TaskExecutionException, ExecutionException {
		ResultSegment segment = context.getResultData().getSegment(this.segmentName);
		
		// What attributes can be removed?
		String[] arAttributesToRemove = {"searchprofile","sortby","sortorder"};
		
		// Evaluate the given navigatorName
		Expression<String> navigatorNameExpression = new Expression<String>(this.navigatorNameExpression);
		this.navigatorName = navigatorNameExpression.evaluate(context);

		// Get query data
		QueryData qd = context.getQueryData();
		
		// A new qd object must be created due to persistent references when creating many elements
		QueryData qdNew = new QueryData();
		
		if (segment != null) {
			try {

				for (ResultPage resultPage : segment.getResultPages()) {
					// Get the wanted navigator
					NavigationEntry navEntry = resultPage.getNavigationEntryByName(this.navigatorName);
					
					if (navEntry != null) {
						// Get the current list of elements
						List<NavigationElement> navElemList = navEntry.getNavigationElements();
						// Clean away unnecessary parameters
						for (NavigationElement navigationElement : navElemList) {
							for (String attrKey : arAttributesToRemove) {
								// Remove specified attributes not needed for links
								navigationElement.getQuery().removeAttributeByKey(attrKey);
							}
						}
						
						// If number of returned items are exactly 11, the navigator has either exactly 11 elements, or have been limited by filter=11 which means a show more link is needed 
						if (navElemList.size()==11) {
							qdNew.setHttpRequestParameters(qd.getHttpRequestParameters());
							for (String attrKey : arAttributesToRemove) {
								// Remove specified attributes not needed for links
								qdNew.removeAttributeByKey(attrKey);
							}
							// Loop through all given parameters in config and replace any previous attribute values
							for (String key : this.parameters.keySet()) {
								parameterValueExpression = new Expression<String>(this.parameters.get(key));
								parameterValue = parameterValueExpression.evaluate(context);
								qdNew.setAttribute(key, parameterValue);
							}
							

							// Create new NavigationElement and specify parameters
							NavigationElement newElem = new NavigationElement();
							newElem.setName(this.elementName);
							newElem.setQuery(qdNew);
							
							// Add created element to cut-off element list 
							navElemList = new ArrayList<NavigationElement>(navElemList.subList(0, 10));
							navElemList.add(newElem);
							navEntry.setNavigationElements(navElemList);							
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
	}

	public void setSegmentName(String segmentName) {
		this.segmentName = segmentName;
	}

	public void setNavigatorNameExpression(String navigatorNameExpression) {
		this.navigatorNameExpression = navigatorNameExpression;
	}

	public void setElementName(String elementName) {
		this.elementName = elementName;
	}

	public void setParameters(Map<String, String> parameters) {
		this.parameters = parameters;
	}
}
