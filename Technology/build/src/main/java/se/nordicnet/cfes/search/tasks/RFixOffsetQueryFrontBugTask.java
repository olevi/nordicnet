package se.nordicnet.cfes.search.tasks;

import java.util.ArrayList;

import no.comperio.fes.search.SearchContext;
import no.comperio.fes.search.common.query.QueryData;
import no.comperio.fes.search.common.result.ResultSegment;
import no.comperio.fes.search.common.result.ResultPage;
import no.comperio.fes.search.tasks.AbstractSearchTask;
import no.comperio.workflowengine.tasks.TaskConfigurationException;
import no.comperio.workflowengine.tasks.TaskExecutionException;
import no.comperio.workflowengine.util.DeepCloner;

/**
 * @author Marcus Johansson
 */
public class RFixOffsetQueryFrontBugTask extends AbstractSearchTask {

	private String segmentName;
	private int cloudSize = -1;
	
	@Override
	public void afterPropertiesSet() throws TaskConfigurationException {
		if (this.segmentName == null) {
			throw new TaskConfigurationException(this, "Parameter 'segmentName' must be set");
		}
		if (this.cloudSize == -1) {
			throw new TaskConfigurationException(this, "Parameter 'cloudSize' must be set");
		}
	}
	
	@Override
	public void executeSearchTask(SearchContext context) throws TaskExecutionException {
		ResultSegment segment = context.getResultData().getSegment(this.segmentName);
		for (ResultPage resultPage : segment.getResultPages()) {
			int resultHitsPerPage = resultPage.getResultSet().getHitsPerPage();
			if( resultHitsPerPage <= 0 ) {
				return;
			}
			
			QueryData query = context.getQueryData();
			int queryPageNumber = query.getPageNumber();
			long resultTotalHits = resultPage.getResultSet().getTotalHits();
	
			// calculate first and last page number for "other" links
			int resultPageCount = (int) (resultTotalHits-1) / resultHitsPerPage;
			int adjustIfAtEnd = Math.max(0, this.cloudSize/2 - (resultPageCount-queryPageNumber)); 	// To handle position on last page. KAR
			int firstOtherPageNumber = Math.max(0, queryPageNumber - this.cloudSize / 2 - adjustIfAtEnd + 1);
			int lastOtherPageNumber = Math.min(resultPageCount, firstOtherPageNumber + this.cloudSize - 1 + adjustIfAtEnd);
	
			ArrayList<QueryData> offsetQueries = new ArrayList<QueryData>();
			for (int i = firstOtherPageNumber; i <= lastOtherPageNumber; i++) {
				QueryData otherPageQuery = (QueryData) DeepCloner.clone(query);
				otherPageQuery.setPageNumber(i);
				offsetQueries.add(otherPageQuery);
			}
			resultPage.setOffsetQueries(offsetQueries);
		}
	}
	
	public void setSegmentName(String segmentName) {
		this.segmentName = segmentName;
	}
	
	public void setCloudSize(int cloudSize) {
		this.cloudSize = cloudSize;
	}	

}
