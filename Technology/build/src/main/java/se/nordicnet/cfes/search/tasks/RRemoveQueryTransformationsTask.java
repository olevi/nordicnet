package se.nordicnet.cfes.search.tasks;

import java.util.List;
import java.util.Map;

import no.comperio.workflowengine.tasks.TaskConfigurationException;
import no.comperio.workflowengine.tasks.TaskExecutionException;
import no.comperio.fes.search.SearchContext;
import no.comperio.fes.search.tasks.AbstractSearchTask;
import no.comperio.fes.search.common.result.ResultSegment;
import no.comperio.fes.search.common.result.ResultPage;
import no.comperio.fes.search.common.result.QueryTransformation;

;

/**
 * @author Marcus Johansson
 */
public class RRemoveQueryTransformationsTask extends AbstractSearchTask {

	private String segmentName;
	private List<String> remove;

	@Override
	public void afterPropertiesSet() throws TaskConfigurationException {
		if (this.remove.size() <= 0) {
			throw new TaskConfigurationException(this, "The field \'remove\' can't be empty");
		}
	}

	@Override
	public void executeSearchTask(SearchContext context) throws TaskExecutionException {
		ResultSegment segment = context.getResultData().getSegment(this.segmentName);
		if (segment != null) {
			for (ResultPage resultPage : segment.getResultPages()) {
				// Get the current QT set
				Map<String, QueryTransformation> qtMap = resultPage.getQueryTransformations();
				
				// Remove those matching our remove list
				for (String qtName : this.remove) {
					qtMap.remove(qtName);
				}

				// Overwrite the old QT set
				resultPage.setQueryTransformations(qtMap);
			}
		}
	}

	public void setSegmentName(String segmentName) {
		this.segmentName = segmentName;
	}
	
	public void setRemove(List<String> remove) {
		this.remove = remove;
	}
}
