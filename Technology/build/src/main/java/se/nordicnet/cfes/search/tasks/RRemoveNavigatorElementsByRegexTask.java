package se.nordicnet.cfes.search.tasks;

import java.util.List;
import java.util.ArrayList;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import no.comperio.fes.search.SearchContext;
import no.comperio.fes.search.common.result.NavigationElement;
import no.comperio.fes.search.common.result.NavigationEntry;
import no.comperio.fes.search.common.result.ResultPage;
import no.comperio.fes.search.common.result.ResultSegment;
import no.comperio.fes.search.tasks.AbstractSearchTask;
import no.comperio.workflowengine.tasks.TaskConfigurationException;
import no.comperio.workflowengine.tasks.TaskExecutionException;
import no.comperio.workflowengine.util.Expression;

/**
 * @author Marcus Johansson
 */
public class RRemoveNavigatorElementsByRegexTask extends AbstractSearchTask {

	private String segmentName;
	private String navigatorName;
	private String regex;
	private Expression<String> regexExpression;
	
	private Pattern regexCompiled;

	@Override
	public void afterPropertiesSet() throws TaskConfigurationException {
		if (this.segmentName == null) {
			throw new TaskConfigurationException(this, "Field 'segmentName' cannot be empty");
		}
		if (this.navigatorName == null) {
			throw new TaskConfigurationException(this, "Field 'navigatorName' cannot be empty");
		}
		if (this.regex == null && this.regexExpression == null) {
			throw new TaskConfigurationException(this, "Both fields 'regex' and 'regexExpression' cannot be empty");
		}
		
		// Compile non-JEXL regex, if available
		if (this.regex != null) {
			try {
				this.regexCompiled = Pattern.compile(this.regex);
			} catch (PatternSyntaxException e) {
				throw new TaskConfigurationException(this, "Pattern " + this.regex + "in field 'regex' does not compile");
			}
		}
	}

	@Override
	public void executeSearchTask(SearchContext context) throws TaskExecutionException {
		// Compile JEXL regex, if used
		if (this.regex == null) {
			this.regexCompiled = Pattern.compile(this.regexExpression.evaluate(context));
		}
		
		ResultSegment segment = context.getResultData().getSegment(this.segmentName);
		if (segment != null) {
			for (ResultPage resultPage : segment.getResultPages()) {
				NavigationEntry navEntry = resultPage.getNavigationEntryByName(this.navigatorName);
				if (navEntry != null) {
					// Get current navigation elements and initialize new list
					List<NavigationElement> navElemList = navEntry.getNavigationElements();
					List<NavigationElement> newNavElemList = new ArrayList<NavigationElement>();

					// Add navigation elements NOT matching the regex to the new list
					for (NavigationElement navElem : navElemList) {
						if (!this.regexCompiled.matcher(navElem.getName()).matches()) {
							newNavElemList.add(navElem);
						}
					}

					navEntry.setNavigationElements(newNavElemList);
				}
			}
		}
	}

	public void setSegmentName(String segmentName) {
		this.segmentName = segmentName;
	}

	public void setNavigatorName(String navigatorName) {
		this.navigatorName = navigatorName;
	}

	public void setRegex(String regex) {
		this.regex = regex;
	}
	
	public void setRegexExpression(String regexExpression) {
		this.regexExpression = new Expression<String>(regexExpression);
	}	

}
