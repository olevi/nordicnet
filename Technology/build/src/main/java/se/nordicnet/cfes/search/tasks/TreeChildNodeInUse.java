package se.nordicnet.cfes.search.tasks;

import java.util.Iterator;

import no.comperio.fes.search.SearchContext;
import no.comperio.fes.search.common.result.NavigationElement;
import no.comperio.fes.search.common.result.NavigationEntry;
import no.comperio.fes.search.common.result.NavigationNode;
import no.comperio.fes.search.common.result.ResultData;
import no.comperio.fes.search.common.result.ResultPage;
import no.comperio.fes.search.common.result.ResultSegment;
import no.comperio.fes.search.tasks.AbstractSearchTask;
import no.comperio.workflowengine.tasks.TaskExecutionException;
import no.comperio.workflowengine.util.Expression;

import org.springframework.util.StringUtils;

public class TreeChildNodeInUse extends AbstractSearchTask {

	private static final String DEFAULT_NAVIGATOR_NAME = "producthierarchy-sve";
	private static final String DEFAULT_SEGMENT_NAME = "companies";
	private static final String ROOT_NAVIGATOR_NAME = "root";

	private String navigatorName;
	
	public void executeSearchTask( final SearchContext ctx ) throws TaskExecutionException {

		ResultData result = ctx.getResultData();
		ResultSegment resultSegment = result.getSegment(DEFAULT_SEGMENT_NAME);

		if (resultSegment != null) {

			for( ResultPage page : resultSegment.getResultPages() ) {

				if (page.getNavigationEntries() != null) {
					Iterator<NavigationEntry> entryIterator = page.getNavigationEntries().iterator();

					while (entryIterator.hasNext()) {
						NavigationEntry navigationEntry = entryIterator.next();
						String navigatorName = navigationEntry.getDisplayName();

						if (navigatorName.equalsIgnoreCase(getNavigatorName(ctx))) {

							Iterator<NavigationNode> nodeIterator = navigationEntry.getNavigationNodes().iterator();
							while (nodeIterator.hasNext()) {
								NavigationNode rootNode = nodeIterator.next();
								String elementNameTemp = rootNode.getLabel();
								if (StringUtils.hasText(elementNameTemp) && elementNameTemp.equalsIgnoreCase(ROOT_NAVIGATOR_NAME)) {
									Iterator<NavigationNode> oneIterator = rootNode.getChildNodes().iterator();
									while (oneIterator.hasNext()) {
										NavigationNode oneNode = oneIterator.next();
										if (oneNode.getNavigationElement() != null) {
											NavigationElement element = oneNode.getNavigationElement();
											if (element.isInUse()) {
												rootNode.setChildInUse(true); oneNode.setChildInUse(true);
											}
										}
										Iterator<NavigationNode> twoIterator = oneNode.getChildNodes().iterator();
										while (twoIterator.hasNext()) {
											NavigationNode twoNode = twoIterator.next();
											if (twoNode.getNavigationElement() != null) {
												NavigationElement element = twoNode.getNavigationElement();
												if (element.isInUse()) {
													rootNode.setChildInUse(true); oneNode.setChildInUse(true); twoNode.setChildInUse(true);
												}
											}
											Iterator<NavigationNode> threeIterator = twoNode.getChildNodes().iterator();
											while (threeIterator.hasNext()) {
												NavigationNode threeNode = threeIterator.next();
												NavigationElement element = threeNode.getNavigationElement();
												if (element.isInUse()) {
													// In order for these methods to become possible (the setChildInUse(true) on NavigationNode)
													// we had to customize this class in Front as well as the front-xml-xstream-adapter.xml.
													rootNode.setChildInUse(true);
													oneNode.setChildInUse(true);
													twoNode.setChildInUse(true);
													threeNode.setChildInUse(true);
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	
	public void setNavigatorName(String navigatorName) {
		this.navigatorName = navigatorName;
	}
	
	public String getNavigatorName(SearchContext ctx) {
		if (!StringUtils.hasText(navigatorName)) { this.navigatorName = DEFAULT_NAVIGATOR_NAME; }
		Expression<String> navigatorNameExpression = new Expression<String>(this.navigatorName);
		String tempNavigatorName = navigatorNameExpression.evaluate(ctx);
		if (!StringUtils.hasText(tempNavigatorName)) { return DEFAULT_NAVIGATOR_NAME; }
		else { return tempNavigatorName; }
	}
}
