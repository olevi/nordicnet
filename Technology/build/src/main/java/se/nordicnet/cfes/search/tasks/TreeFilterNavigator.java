package se.nordicnet.cfes.search.tasks;

import java.util.Iterator;

import no.comperio.fes.search.SearchContext;
import no.comperio.fes.search.common.result.NavigationEntry;
import no.comperio.fes.search.common.result.NavigationNode;
import no.comperio.fes.search.common.result.ResultData;
import no.comperio.fes.search.common.result.ResultPage;
import no.comperio.fes.search.common.result.ResultSegment;
import no.comperio.fes.search.tasks.AbstractSearchTask;
import no.comperio.workflowengine.tasks.TaskExecutionException;
import no.comperio.workflowengine.util.Expression;

import org.springframework.util.StringUtils;

/**
 * This class filters the content of the navigator (producthierarchy-sve) based on the filter string provided to
 * the filter= attribute in the URL.
 * @author Daniel Ling (daniel.ling@comperio.no)
 */
public class TreeFilterNavigator extends AbstractSearchTask {

	private static final String DEFAULT_NAVIGATOR_NAME = "producthierarchy-sve";
	private static final String FILTER_ATTRIBUTE_NAME = "filter";
	private static final String DEFAULT_SEGMENT_NAME = "companies";
	private static final String ROOT_NAVIGATOR_NAME = "root";

	private String navigatorName;

	public void executeSearchTask( final SearchContext ctx ) throws TaskExecutionException {

		String filter = (String) ctx.getQueryData().getAttribute(FILTER_ATTRIBUTE_NAME);

		if (StringUtils.hasText(filter)) {

			filter = filter.toLowerCase();
			
			ResultData result = ctx.getResultData();
			ResultSegment resultSegment = result.getSegment(DEFAULT_SEGMENT_NAME);

			if (resultSegment != null) {

				for( ResultPage page : resultSegment.getResultPages() ) {

					if (page.getNavigationEntries() != null) {
						Iterator<NavigationEntry> entryIterator = page.getNavigationEntries().iterator();

						while (entryIterator.hasNext()) {
							NavigationEntry navigationEntry = entryIterator.next();
							String navigatorName = navigationEntry.getDisplayName();

							if (navigatorName.equalsIgnoreCase(getNavigatorName(ctx))) {

								Iterator<NavigationNode> nodeIterator = navigationEntry.getNavigationNodes().iterator();

								while (nodeIterator.hasNext()) {
									NavigationNode rootNode = nodeIterator.next();
									String elementNameTemp = rootNode.getLabel();
									if (StringUtils.hasText(elementNameTemp) && elementNameTemp.equalsIgnoreCase(ROOT_NAVIGATOR_NAME)) {

										Iterator<NavigationNode> oneIterator = rootNode.getChildNodes().iterator();
										while (oneIterator.hasNext()) {
											NavigationNode oneNode = oneIterator.next();
											boolean keepCurrentOne = false;
											if (oneNode.getLabel().toLowerCase().contains(filter)) {
												keepCurrentOne = true;
											}
											Iterator<NavigationNode> twoIterator = oneNode.getChildNodes().iterator();
											while (twoIterator.hasNext()) {
												NavigationNode twoNode = twoIterator.next();
												boolean keepCurrentTwo = false;
												if (twoNode.getLabel().toLowerCase().contains(filter)) {
													keepCurrentOne = true;
													keepCurrentTwo = true;
												}
												Iterator<NavigationNode> threeIterator = twoNode.getChildNodes().iterator();
												while (threeIterator.hasNext()) {
													NavigationNode threeNode = threeIterator.next();
													if (threeNode.getLabel().toLowerCase().contains(filter)) {
														keepCurrentOne = true;
														keepCurrentTwo = true;
													}
													else { threeIterator.remove(); }
												}
												if (!keepCurrentTwo) { twoIterator.remove(); }
											}
											if (!keepCurrentOne) { oneIterator.remove(); }
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	
	public void setNavigatorName(String navigatorName) {
		this.navigatorName = navigatorName;
	}
	
	public String getNavigatorName(SearchContext ctx) {
		if (!StringUtils.hasText(navigatorName)) { this.navigatorName = DEFAULT_NAVIGATOR_NAME; }
		Expression<String> navigatorNameExpression = new Expression<String>(this.navigatorName);
		String tempNavigatorName = navigatorNameExpression.evaluate(ctx);
		if (!StringUtils.hasText(tempNavigatorName)) { return DEFAULT_NAVIGATOR_NAME; }
		else { return tempNavigatorName; }
	}
}
