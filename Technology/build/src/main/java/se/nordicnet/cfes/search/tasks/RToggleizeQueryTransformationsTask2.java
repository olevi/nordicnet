package se.nordicnet.cfes.search.tasks;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import no.comperio.workflowengine.tasks.TaskConfigurationException;
import no.comperio.workflowengine.tasks.TaskExecutionException;
import no.comperio.fes.search.SearchContext;
import no.comperio.fes.search.tasks.AbstractSearchTask;
import no.comperio.fes.search.common.query.QueryData;
import no.comperio.fes.search.common.result.ResultSegment;
import no.comperio.fes.search.common.result.ResultPage;
import no.comperio.fes.search.common.result.QueryTransformation;

/**
 * @author Pontus Hym�r
 * 
 * @description Reduces number of query transformation nodes by creating "toggling" versions of them. 
 */
public class RToggleizeQueryTransformationsTask2 extends AbstractSearchTask {

	private String segmentName;

	@Override
	public void afterPropertiesSet() throws TaskConfigurationException {
		if (this.segmentName == null) {
			throw new TaskConfigurationException(this,
					"Parameter 'segmentName' must be set");
		}
	}

	@Override
	public void executeSearchTask(SearchContext context) throws TaskExecutionException {
		
		ResultSegment segment = context.getResultData().getSegment(this.segmentName);
		QueryData qd = context.getQueryData();

		String strCurrentSortBy = "";
		String strCurrentSortOrder = "";

		String strItemSortBy;
		String strItemSortOrder;
		String strItemToggleName;

		QueryTransformation qtItem;
		QueryData queryItem;

		strCurrentSortBy = (qd.getSortBy()!=null)?qd.getSortBy():"";
		strCurrentSortOrder = (qd.getSortOrder()!=null)?qd.getSortOrder():"ASC";
		
		// Remove countrycodes from sortby fields.
		// TODO: Can requested fieldname be extracted from somewhere else? Or put regexp as in-parameter
		String regex = "(sve)|(nor)|(fin)|(dan)|(eng)";
		Pattern p = Pattern.compile(regex);
		java.util.regex.Matcher m = p.matcher(strCurrentSortBy); 
		strCurrentSortBy = m.replaceAll("");
		strCurrentSortBy = strCurrentSortBy.replace("countryname", "country");
		
		if (segment != null) {
			ResultPage resultPage = segment.getResultPages().get(0);
			// Get the current QT set
			Map<String, QueryTransformation> qtMapIn = resultPage.getQueryTransformations();
			Map<String, QueryTransformation> qtMapOut = new HashMap<String, QueryTransformation>(qtMapIn);
			Map<String, QueryTransformation> qtMapDidYouMeans = new HashMap<String, QueryTransformation>(qtMapIn);
			qtMapDidYouMeans.clear();
			
			qtMapOut.clear();

			// Loop through all qts
			for (String qtName : qtMapIn.keySet()) {
				// Only handle sorting QTs here, add the rest
				// Get the qt being looped through
				qtItem = qtMapIn.get(qtName);
				if (qtName.contains("sortBy")) {
					queryItem = qtItem.getQuery();
					// Get QT parameters
					strItemSortBy = (queryItem.getSortBy()!=null)? queryItem.getSortBy(): "relevance";
					strItemSortOrder = (queryItem.getSortOrder()!="")? queryItem.getSortOrder(): "ASC";
					// Set new name for QT
					strItemToggleName = qtName.replace("Asc", "").replace("Desc", "");
					
					// Only continue if QT is not yet in collection of wanted QTs
					if (!qtMapOut.containsKey(strItemToggleName)) {
						qtItem.setName(strItemToggleName);
						// Clean QT up
						queryItem.removeAttributeByKey("sortOrder");
						queryItem.removeAttributeByKey("sortBy");
						// Make sure to set correct parameters
						queryItem.setAttribute("sortby", strItemSortBy);
						queryItem.setAttribute("sortorder", strItemSortOrder);
						// Is this needed?
						queryItem.removeAttributeByKey("mycustomattribute");
						// Indicate sortorder in custom-attribute
						qtItem.setCustom(strItemSortOrder);
						// Set modified query to QT
						qtItem.setQuery(queryItem);

						// Add QT to resulting QT collection if it's not the current one, unless it's relevance
						if (!(strItemSortBy.equalsIgnoreCase(strCurrentSortBy) && strItemSortOrder.equalsIgnoreCase(strCurrentSortOrder)) || strItemSortBy=="") {
							qtMapOut.put(strItemToggleName, qtItem);
						}
					}
				}
//				else if (qtName.contains("DID_YOU_MEAN")) {
//					qtMapDidYouMeans.put(qtName, qtItem);
//				}
				else
					qtMapOut.put(qtName, qtItem);
			}
			// Sort out DidYouMean QTFs to ONLY display last active DID_YOU_MEAN
//			if(qtMapDidYouMeans.size()>0){
//				List mapKeys = new ArrayList(qtMapDidYouMeans.keySet());
//				Collections.sort(mapKeys);
//				qtItem = qtMapIn.get(mapKeys.get(mapKeys.size()-1));
//				queryItem = qtItem.getQuery();
//				queryItem.setAttribute("what",queryItem.getAttribute("querystring").toString());
//				//queryItem.removeAttributeByKey("querystring");
//				qtItem.setQuery(queryItem);
//				qtItem.setName("DID_YOU_MEAN");
//				qtMapOut.put("DID_YOU_MEAN",qtItem);
//			}
			
			// Overwrite the old QT set
			resultPage.setQueryTransformations(qtMapOut);
		}
	}

	public void setSegmentName(String segmentName) {
		this.segmentName = segmentName;
	}
}
