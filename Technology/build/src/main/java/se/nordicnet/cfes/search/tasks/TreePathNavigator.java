package se.nordicnet.cfes.search.tasks;

import java.util.Iterator;

import no.comperio.fes.search.SearchContext;
import no.comperio.fes.search.common.result.NavigationEntry;
import no.comperio.fes.search.common.result.NavigationNode;
import no.comperio.fes.search.common.result.ResultData;
import no.comperio.fes.search.common.result.ResultPage;
import no.comperio.fes.search.common.result.ResultSegment;
import no.comperio.fes.search.tasks.AbstractSearchTask;
import no.comperio.workflowengine.tasks.TaskExecutionException;
import no.comperio.workflowengine.util.Expression;

import org.springframework.util.StringUtils;

/**
 * This class makes sure that the returned product hierachy is in sync with the given path, such as path=Motorfordon_och_tillbeh%C3%B6r%24Motorfordon. 
 * @author Daniel Ling (daniel.ling@comperio.no)
 */
public class TreePathNavigator extends AbstractSearchTask {
	
	private static final String DEFAULT_NAVIGATOR_NAME = "producthierarchy-sve";
	private static final String DEFAULT_ATTRIBUTE_NAME = "path";
	private static final String DEFAULT_SEGMENT_NAME = "companies";
	private static final String ROOT_NAVIGATOR_NAME = "root";
	private static final String SPECIAL_IGNORED_CHAR = "*";
	
	private String navigatorName;
	
	public void executeSearchTask( final SearchContext ctx ) throws TaskExecutionException {
				
		String pathString = (String) ctx.getQueryData().getAttribute(DEFAULT_ATTRIBUTE_NAME);
		
		if (StringUtils.hasText(pathString)) {
			
			String[] pathSplit = pathString.split("\\$");
			
			ResultData result = ctx.getResultData();
			ResultSegment resultSegment = result.getSegment(DEFAULT_SEGMENT_NAME);
			
			if (resultSegment != null && !pathString.equalsIgnoreCase(SPECIAL_IGNORED_CHAR)) {

				for( ResultPage page : resultSegment.getResultPages() ) {
					
					if (page.getNavigationEntries() != null) {
						Iterator<NavigationEntry> entryIterator = page.getNavigationEntries().iterator();

						while (entryIterator.hasNext()) {
							NavigationEntry navigationEntry = entryIterator.next();
							String navigatorName = navigationEntry.getDisplayName();
							
							if (navigatorName.equalsIgnoreCase(getNavigatorName(ctx))) {
								
								Iterator<NavigationNode> nodeIterator = navigationEntry.getNavigationNodes().iterator();
								
								while (nodeIterator.hasNext()) {
									NavigationNode oneElement = nodeIterator.next();
									String elementNameTemp = oneElement.getLabel();
									if (StringUtils.hasText(elementNameTemp) && elementNameTemp.equalsIgnoreCase(ROOT_NAVIGATOR_NAME)) {
										
										for (int i = 0; i < pathSplit.length; i++) {
											Iterator<NavigationNode> nodeIterate = oneElement.getChildNodes().iterator();
											while (nodeIterate.hasNext()) {
												oneElement = nodeIterate.next();
												if (!oneElement.getLabel().equalsIgnoreCase(pathSplit[i])) {
													nodeIterate.remove();
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	
	public void setNavigatorName(String navigatorName) {
		this.navigatorName = navigatorName;
	}
	
	public String getNavigatorName(SearchContext ctx) {
		if (!StringUtils.hasText(navigatorName)) { this.navigatorName = DEFAULT_NAVIGATOR_NAME; }
		Expression<String> navigatorNameExpression = new Expression<String>(this.navigatorName);
		String tempNavigatorName = navigatorNameExpression.evaluate(ctx);
		if (!StringUtils.hasText(tempNavigatorName)) { return DEFAULT_NAVIGATOR_NAME; }
		else { return tempNavigatorName; }
	}
}
