package se.nordicnet.cfes.search.tasks;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import no.comperio.fes.search.SearchContext;
import no.comperio.fes.search.adapter.QueryAnalyserXMLConverter;
import no.comperio.fes.search.common.query.QueryData;
import no.comperio.fes.search.common.result.QueryAnalyserMatch;
import no.comperio.fes.search.tasks.AbstractSearchTask;
import no.comperio.workflowengine.tasks.TaskExecutionException;
import no.comperio.workflowengine.util.Expression;

import org.apache.log4j.Logger;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
/**
 * <strong>Non-technical:  </strong><br>
 * This task will enable matching of the query term against a set of rules, regular expressions or dictionaries. User intent can 
 * be determined by the type of query submitted, offering a chance to alter the way the query is executed and the result presented.
 *
 * 
 * <br><br>
 * <strong>Technical:  </strong><br>
 * The QueryData.queryString will be sent to the matcherserver and the results (of type
 * no.comperio.fes.search.common.result.QueryAnalyserMatch) put in an ArrayList.
 * Use these values in subsequent task to alter behaviour. The ArrayList will be stored in the context-object
 * under the attributeName configured.
 *
 * <p>
 * <strong> Input parameters: </strong> <br>
 * <table cellspadding="5" border="1">
 * <tr>
 * <th align="left">Parameter Name</th>
 * <th align="left">Description</th>
 * <th align="left">Type</th>
 * <th align="left">Default</th>
 * <th align="left">Optional</th>
 * </tr>
 * <tr>
 * <td> <code>qaHost</code> </td>
 * <td> Queryanalyser/matcherserver-host. Include http:// in front</td>
 * <td> <code>java.lang.String</code></td>
 * <td> <code>null</code> </td>
 * <td> No</td>
 * </tr>
 * <td> <code>queryPrefix</code> </td>
 * <td> Normally /search?q=. This is what is concatenated between the host and the queryterm when constructing the http-request</td>
 * <td> <code>java.lang.String</code></td>
 * <td> <code>null</code> </td>
 * <td> No</td>
 * </tr>
 * <td> <code>parserMatchString</code> </td>
 * <td> Deprecated - not in use</td>
 * <td> <code>java.lang.String</code></td>
 * <td> <code>null</code> </td>
 * <td> Yes</td>
 * </tr>
 * <td> <code>attributeName</code> </td>
 * <td> Name of Arraylist containing all <code>QueryAnalyserMatch</code> objects</td>
 * <td> <code>java.lang.String</code></td>
 * <td> <code>null</code> </td>
 * <td> No</td>
 * </tr>
 * <td> <code>queryData</code> </td>
 * <td> Optional name of query data object in search context</td>
 * <td> <code>java.lang.String</code></td>
 * <td> <code>null</code> </td>
 * <td> Yes</td>
 * </tr>
 * </table>
 * </p>
 *  
 * <br><br>
 * <strong>Example: </strong>
 *
 * <pre> &lt;searchTask:QFetchQueryMatcherValuesTask qaHost="http://admin01.test.fast.tinde.no:15300" 
 * 	queryPrefix="/search?q=" attributeName="qmresult" queryData="q1" /&gt; </pre>
 *
 * @since v1.0 - May, 2007
 * @author Ole-Kr. Villab&oslash;, Comperio AS
 *
 */
public class QFetchQueryMatcherValuesTask extends AbstractSearchTask {
	
	/**
	 * queryanalyser/matcherserver-host. Include http:// in front
	 */
	private String qaHost;
	/**
	 * normally /search?q=. This is what is concatenated between the host and the queryterm when constructing the http-request
	 */
	private String queryPrefix;
	private String querySuffix;
	/**
	 * Deprecated - not in use
	 */
	private String parserMatchString;
	/**
	 * Name of Arraylist containing all <code>QueryAnalyserMatch</code> objects
	 */
	private String attributeName;
	
	private Expression<QueryData> queryDataExpression;

	private static final String ENCODING = "UTF-8";

	private Logger log = Logger.getLogger(QFetchQueryMatcherValuesTask.class);

	/**
	 * This method will put a java.util.List of QueryAnalyserMatch-objects in an
	 * attribute with key value attributeName
	 * 
	 */
	@Override
	public void executeSearchTask(SearchContext context) throws TaskExecutionException {
		QueryData qd = queryDataExpression != null ?queryDataExpression.evaluate(context) : context.getQueryData();
		String query = qd.getQueryString();

		resolveQueryAnalysis(context, query);
	}

	/**
	 * This method will query the QueryMatcher / QueryAnalyser based on the
	 * input attributes qaHost and queryPrefix as well as the queryitself. The
	 * context-Attribute with name from attribute attributeNames will also be
	 * set as a java.util.ArrayList<QueryAnalyserMatch>
	 */
	private void resolveQueryAnalysis(SearchContext context, String query) throws TaskExecutionException {

		String xmlOutput = null;
		try {
			xmlOutput = getQueryAnalyserOutput(context, query);
		} catch (IOException ioex) {
			log.error("Could not read from Analysisserver: "+qaHost);
		}

		if (xmlOutput == null) {
			return;
		} else {
			ArrayList<QueryAnalyserMatch> matchList = (ArrayList<QueryAnalyserMatch>) getQueryAnalyserMatches(context, xmlOutput);

			storeAttribute(context, matchList);
		}
	}

	/**
	 * Override this method to change how the querymatcher-values are stored in
	 * the context attribute.
	 */
	protected void storeAttribute(SearchContext context, ArrayList<QueryAnalyserMatch> matchList) throws TaskExecutionException {

		context.setAttribute(this.attributeName, matchList);
		if (log.isDebugEnabled()) {
			log.debug("storing querymatcher values in context attribute : " + this.attributeName);
		}
	}

	@SuppressWarnings("unchecked")
	private List<QueryAnalyserMatch> getQueryAnalyserMatches(SearchContext context, String xml) {

		QueryAnalyserXMLConverter converter = new QueryAnalyserXMLConverter();
		XStream xstream = new XStream(new DomDriver());
		xstream.alias("matches", QueryAnalyserMatch.class);

		xstream.registerConverter(converter);

		return (List<QueryAnalyserMatch>) xstream.fromXML(xml);
	}

	private String encode(String query) throws TaskExecutionException {
		try {
			String queryLC = query.toLowerCase();
			String queryEncoded = URLEncoder.encode(URLDecoder.decode(queryLC, ENCODING), ENCODING);
			return queryEncoded;
		} catch (UnsupportedEncodingException ex) {
			throw new TaskExecutionException(this, ex.getMessage());
		}
	}

	private String getQueryAnalyserOutput(SearchContext context, String query) throws IOException, TaskExecutionException {
		StringBuffer xml = new StringBuffer();
		String queryEncoded = encode(query);

		URL cfes = new URL(qaHost + queryPrefix + queryEncoded + querySuffix);

		BufferedReader in = new BufferedReader(new InputStreamReader(cfes.openStream(), ENCODING));

		String inputLine;

		while ((inputLine = in.readLine()) != null) {
			xml.append(inputLine);
		}
		in.close();

		log.debug(xml);

		return xml.toString();

	}

	/**
	 * Sets the queryDataExpression
	 * @param expression the expression
	 */
	public void setQueryData(String expression ) {
		this.queryDataExpression = new Expression<QueryData>( expression );
	}
	
	/**
	 * Returns the qaHost
	 * @return qaHost
	 */
	public String getQaHost() {
		return qaHost;
	}

	/**
	 * Sets the qaHost
	 * @param qaHost the qaHost to set
	 */
	public void setQaHost(String qaHost) {
		this.qaHost = qaHost;
	}
	/**
	 * Returns the query prefix
	 * @return queryPrefix
	 */
	public String getQuerySuffix() {
		return queryPrefix;
	}

	/**
	 * Sets the queryPrefix
	 * @param queryPrefix the queryPrefix to set
	 */
	public void setQuerySuffix(String querySuffix) {
		this.querySuffix= querySuffix;
	}
	/**
	 * Returns the query prefix
	 * @return queryPrefix
	 */
	public String getQueryPrefix() {
		return queryPrefix;
	}

	/**
	 * Sets the queryPrefix
	 * @param queryPrefix the queryPrefix to set
	 */
	public void setQueryPrefix(String queryPrefix) {
		this.queryPrefix = queryPrefix;
	}

	/**
	 * Returns the parserMatchString
	 * @return parserMatchString
	 */
	public String getParserMatchString() {
		return parserMatchString;
	}

	/**
	 * Sets the parserMatchString
	 * @param parserMatchString the parserMatchString to set         
	 */
	public void setParserMatchString(String parserMatchString) {
		this.parserMatchString = parserMatchString;
	}

	/**
	 * Returns the attributeName
	 * @return attributeName
	 */
	public String getAttributeName() {
		return attributeName;
	}

	/**
	 * Sets the attributeName
	 * @param attributeName the attributeName to set
	 */
	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}
}
