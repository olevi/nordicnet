package se.nordicnet.cfes.search.tasks;

import java.util.List;

import no.comperio.fes.search.SearchContext;
import no.comperio.fes.search.common.query.QueryData;
import no.comperio.fes.search.common.result.NavigationElement;
import no.comperio.fes.search.common.result.NavigationEntry;
import no.comperio.fes.search.common.result.ResultPage;
import no.comperio.fes.search.common.result.ResultSegment;
import no.comperio.fes.search.tasks.AbstractSearchTask;
import no.comperio.workflowengine.exceptions.ExecutionException;
import no.comperio.workflowengine.tasks.TaskConfigurationException;
import no.comperio.workflowengine.tasks.TaskExecutionException;

/**
 * @author Marcus Johansson
 */
public class RInjectShowAllNavigationElementTask extends AbstractSearchTask {

	private String segmentName;
	private String navigatorName;
	private String name;
	private String parameterName;
	private String parameterValue;

	@Override
	public void afterPropertiesSet() throws TaskConfigurationException {
		if (this.segmentName == null) {
			throw new TaskConfigurationException(this, "Parameter 'segmentName' must be set");
		}
		if (this.navigatorName == null) {
			throw new TaskConfigurationException(this, "Parameter 'navigatorName' must be set");
		}
		if (this.name == null) {
			throw new TaskConfigurationException(this, "Parameter 'name' must be set");
		}
		if (this.parameterName == null) {
			throw new TaskConfigurationException(this, "Parameter 'parameterName' must be set");
		}
		if (this.parameterValue == null) {
			throw new TaskConfigurationException(this, "Parameter 'parameterValue' must be set");
		}
	}

	@Override
	public void executeSearchTask(SearchContext context) throws TaskExecutionException, ExecutionException {
		ResultSegment segment = context.getResultData().getSegment(this.segmentName);
		if (segment != null) {

			for (ResultPage resultPage : segment.getResultPages()) {
				NavigationEntry navEntry = resultPage.getNavigationEntryByName(this.navigatorName);

				if (navEntry != null) {
					List<NavigationElement> navElemList = navEntry.getNavigationElements();

					QueryData qd = context.getQueryData();
					qd.setAttribute(this.parameterName, this.parameterValue);

					// Create new NavigationElement
					NavigationElement newElem = new NavigationElement();
					newElem.setName(this.name);
					newElem.setQuery(qd);

					navElemList.add(newElem);
					navEntry.setNavigationElements(navElemList);
				}
			}
		}
	}

	public void setSegmentName(String segmentName) {
		this.segmentName = segmentName;
	}

	public void setNavigatorName(String navigatorName) {
		this.navigatorName = navigatorName;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public void setParameterName(String parameterName) {
		this.parameterName = parameterName;
	}
	
	public void setParameterValue(String parameterValue) {
		this.parameterValue = parameterValue;
	}

}
