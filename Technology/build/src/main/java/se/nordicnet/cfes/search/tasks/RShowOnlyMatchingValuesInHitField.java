package se.nordicnet.cfes.search.tasks;

import no.comperio.fes.search.SearchContext;
import no.comperio.fes.search.common.result.Hit;
import no.comperio.fes.search.common.result.ResultSegment;
import no.comperio.fes.search.tasks.AbstractSearchTask;
import no.comperio.workflowengine.exceptions.ExecutionException;
import no.comperio.workflowengine.tasks.TaskConfigurationException;
import no.comperio.workflowengine.tasks.TaskExecutionException;

public class RShowOnlyMatchingValuesInHitField extends AbstractSearchTask {

	private String segmentName;
	private String fieldName;
	private String separator = "#";
	private String juniperStartMatchString;

	@Override
	public void afterPropertiesSet() throws TaskConfigurationException {
		if (this.segmentName == null) {
			throw new TaskConfigurationException(this, "Field 'segmentName' cannot be empty");
		}
		if (this.fieldName == null) {
			throw new TaskConfigurationException(this, "Field \'fieldName\' cannot be empty");
		}
		if (this.juniperStartMatchString == null) {
			throw new TaskConfigurationException(this, "Field \'juniperStartMatchString\' cannot be empty");
		}
	}

	@Override
	public void executeSearchTask(SearchContext context) throws TaskExecutionException, ExecutionException {
		ResultSegment segment = context.getResultData().getSegment(this.segmentName);
		if (segment != null) {
			if (!segment.getResultPages().isEmpty()) {
				for (Hit hit : segment.getResultPages().get(0).getResultSet().getHits()) {
					String value = hit.getFieldValue(this.fieldName);
					if (value != null) {

						// Placeholder for filtered content, if no matches this should be ""
						String resultValue = "";
						
						// Split the multi-value field
						for (String internalValue : value.split(this.separator)) {
							// Only keep internalValues matching juniper's highlighting tag
							if(internalValue.contains(this.juniperStartMatchString)) {
						
								// NNET: Also remove productids if field is 'products' 
								if (this.fieldName.equals("products") && internalValue.contains("$")) {
									internalValue = internalValue.substring(0,internalValue.indexOf("$"));
								}
								
								resultValue += internalValue + this.separator;
							}
						}
						
						// Remove the trailing separator, if there is one
						if (resultValue.length() > 0) {
							resultValue = resultValue.substring(0, resultValue.length() - this.separator.length());
						}
						
						// Write content (potentially an empty string) back to field
						hit.setFieldValue(this.fieldName, resultValue);
					}
				}
			}
		}
	}

	public void setSegmentName(String segmentName) {
		this.segmentName = segmentName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public void setSeparator(String separator) {
		this.separator = separator;
	}

	public void setJuniperStartMatchString(String juniperStartMatchString) {
		this.juniperStartMatchString = juniperStartMatchString;
	}

}
